<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\PriceQuote;
use App\QuoteQueue;
use App\Membership;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PriceQuoteRequest;
class PriceQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $PriceQuotes=PriceQuote::orderBy('id', 'DESC')->orderBy('id','DESC')->get();
        
        return view('admin.pricequote',compact('PriceQuotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PriceQuoteRequest $request)
    {
        //
        $phone=$request->phone1.'-'.$request->phone2.'-'.$request->phone3;
        
        $PriceQuote=New PriceQuote;

        $PriceQuote->name               =   $request->name;
        $PriceQuote->email              =   $request->email;
        $PriceQuote->phone              =   $phone;
        $PriceQuote->date_of_event      =   $request->date_of_event;
        $PriceQuote->state              =   $request->state;
        $PriceQuote->city               =   $request->city;
        $PriceQuote->event_type         =   $request->event_type;

        $PriceQuote->save();

        $request->session()->flash('alert-success', 'Qualified vendors near your area will be getting in touch with you soon');

        return redirect('/');

    }
    public function storeQueue(Request $request)
    {
        //
        $phone=$request->phone1.'-'.$request->phone2.'-'.$request->phone3;
        
        $PriceQuote=PriceQuote::find($request->quote_id);

       
        $PriceQuote->status             =   'pending';
       

        $PriceQuote->save();

        $request->session()->flash('alert-success', 'Qualified vendors near your area will be getting in touch with you soon');

        return redirect('/admin/pricequote');

    } 
    public function cronjob()
    {
        // Get all price Quote With Status pending and Sent
      $PriceQuotes=PriceQuote::where('status','=','pending')
                                ->get();
        
       
       
        foreach ($PriceQuotes as $PriceQuote) {

            $checkUserInArea=$PriceQuote->UserMetroCity()
                                    ->whereHas('company',
                                        function($q2)
                                        {
                                            $q2->whereHas('membership_type',
                                                function($q3)
                                                {
                                                    $q3->whereIn('id',[2,3,4,5,6]);
                                                }
                                            );
                                        }
                                    )
                                    ->count();
             // echo $checkUserInArea;die;
            //if user found then run cron for mail send
            if($checkUserInArea>0){

                // Get all Price Quote Queue have a record with Membership 1,4,5,6   
                   
                $countqueue=QuoteQueue::where('quote_id','=',$PriceQuote->id)
                                        ->whereIn('membership_id',[2,3,4,5,6])->count();
                if($countqueue==0){

                    $users=$PriceQuote->UserMetroCity()->whereHas('company',
                                                            function($q2){
                                                                $q2->whereHas('membership_type',function($q3){
                                                                    $q3->whereIn('id',[3,4,5,6]);
                                                                });})->get();
                    if(count($users)==0)
                    {
                        $users=$PriceQuote->UserMetroCity()->whereHas('company',
                                                            function($q2){
                                                                $q2->whereHas('membership_type',function($q3){
                                                                    $q3->whereIn('id',[2]);
                                                                });})->orderBy(DB::raw('RAND()'))->take(10)->get();
                    }

                }
               
                if($countqueue==0){
                    foreach ($users as $user) 
                    {
                         $member=$user->company->membership;

                            $QuoteQueue=New QuoteQueue;
                            $QuoteQueue->quote_id           =   $PriceQuote->id;

                            $QuoteQueue->membership_id      =   $member;
                            $QuoteQueue->userid             =   $user->id;
                            $QuoteQueue->save();

                            $updatePriceQuoteStatus=PriceQuote::find($PriceQuote->id);

                            $updatePriceQuoteStatus->status     =   "Send";

                            $updatePriceQuoteStatus->save();
                           // mail($user->email,'test','test','noreply@bosworldwide.com');
                             $sent = \Mail::send('email.mailquote', ['name'       =>  $PriceQuote->name,
                                                'email'      =>  $PriceQuote->email,
                                                'phone'      =>  $PriceQuote->phone,
                                                'event_date' =>  $PriceQuote->date_of_event ,
                                                'event_type' =>  $PriceQuote->event_type,
                                                'text' =>'test email'
                                                ], 
                                                function($message) use($user){
                                                    $message->to($user->email)
                                                    ->subject('Price Quote Lead!')
                                                    ->from('No-Reply@ThePhotoBoothDirectory.com');
                                                   $message->getHeaders()->addTextHeader('Return-Path', 'do-not-reply@ThePhotoBoothDirectory.com'); 
                                                }
                                );

                    }
                }
            }
            else{
                    $updatePriceQuoteStatus=PriceQuote::find($PriceQuote->id);

                    $updatePriceQuoteStatus->status     =   "No User Found";

                    $updatePriceQuoteStatus->save();
            }

           
        }
        $sent = \Mail::send('email.mailquote', ['name'       =>  $PriceQuote->name,
                                            'email'      =>  $PriceQuote->email,
                                            'phone'      =>  $PriceQuote->phone,
                                            'event_date' =>  $PriceQuote->date_of_event ,
                                            'event_type' =>  $PriceQuote->event_type,
                                            'text' =>'test email'
                                            ], 
                                            function($message){
                                                $message->to('jainpiyush68@gmail.com')
                                                ->subject('Price Quote Lead!')
                                                ->from('No-Reply@ThePhotoBoothDirectory.com'); 
                                                $message->getHeaders()->addTextHeader('Return-Path', 'do-not-reply@ThePhotoBoothDirectory.com');
                                            }
                            );
          if($sent)
                     {
                         echo "send mail sucess";
                        }
                    else{
                       echo "send mail failed";
                    }
    }
     
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $PriceQuote = PriceQuote::find($id);

        return view('admin.editpricequote',compact('PriceQuote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $phone=$request->phone1.'-'.$request->phone2.'-'.$request->phone3;
        
        $PriceQuote=PriceQuote::find($id);

        $PriceQuote->name               =   $request->name;
        $PriceQuote->email              =   $request->email;
        $PriceQuote->phone              =   $phone;
        $PriceQuote->date_of_event      =   $request->date_of_event;
        $PriceQuote->state              =   $request->state;
        $PriceQuote->city               =   $request->city;
        $PriceQuote->event_type         =   $request->event_type;

        $PriceQuote->save();

        $request->session()->flash('alert-success', 'Your Price Quote Successfully Edited');

        return redirect('admin/pricequote/edit/'.$id);
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $PriceQuote = PriceQuote::find($id);

        $PriceQuote->delete();

        return redirect('/admin/pricequote');
    }
    public function approveall(Request $request)
    {
        //
         $memberids=explode(',',$request->allmembers);
         
        for($i=0;$i<count($memberids);$i++){
            $Company=PriceQuote::find($memberids[$i]);
            $Company->status='pending';
            $Company->save();   
        }
        //Session::flash('alert-success', 'Members Was Approved');
        return redirect('admin/pricequote');
    }
   public function removeall(Request $request)
    {
        //
         $allpricequotes=explode(',',$request->allpricequotes);
         
        for($i=0;$i<count($allpricequotes);$i++){
            $removeallpricequotes=PriceQuote::find($allpricequotes[$i]);
            //$allpricequotes->status='pending';
            $removeallpricequotes->delete();   
        }
        //Session::flash('alert-success', 'Members Was Approved');
        return redirect('admin/pricequote');
    }
}