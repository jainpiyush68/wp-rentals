<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Company;
use App\Pricing;
use DateTime;
use Session;
use App\UserAction;
use Helper;
use App\Dj_email_templates;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $profilepaystatus="Active";
        
       if(Input::get('PayerID')){
            $pricing = Pricing::find(1);
            $membership = Input::get('membership');
            $date2=date_create("today");
            date_add($date2,date_interval_create_from_date_string("1 year"));
            if($membership==1):
               $discountprice=$pricing['pr_free']-$pricing['up_free'];
               $saveprice=$pricing['pr_free'];
            elseif($membership==2):
               $discountprice=$pricing['pr_basic']-$pricing['up_basic'];
               $saveprice=$pricing['pr_basic'];
            elseif($membership==3):
                $discountprice=$pricing['pr_bronze']-$pricing['up_bronze'];
                $saveprice=$pricing['pr_bronze'];
            elseif($membership==4):
                $discountprice=$pricing['pr_silver']-$pricing['up_silver'];
                $saveprice=$pricing['pr_silver'];
            elseif($membership==5):
                $discountprice=$pricing['pr_gold']-$pricing['up_gold'];
                $saveprice=$pricing['pr_gold'];
            elseif($membership==6):
                $discountprice=$pricing['pr_platinum']-$pricing['up_platinum'];
                $saveprice=$pricing['pr_platinum'];
            endif;

             $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
              
              
                'METHOD' => 'ManageRecurringPaymentsProfileStatus',
                'VERSION' => '108',
                
              
                
                'PROFILEID' => Auth::user()->company->profileid,
              
                'ACTION' => 'Cancel'
            )));
              
            $response =    curl_exec($curl);
              
            curl_close($curl);
              
            $cancelcheck = array();
              
            if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                foreach ($matches['name'] as $offset => $name) {
                    $cancelcheck [$name] = urldecode($matches['value'][$offset]);
                }
            }
            
             $curl = curl_init();
  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'DoExpressCheckoutPayment',
            'VERSION' => '93 ',
          
           
           
          
            'TOKEN' => Input::get('token'),

            'PAYERID'=> Input::get('PayerID'),
            'PAYMENTREQUEST_0_PAYMENTACTION'=>'SALE',
            'PAYMENTREQUEST_0_AMT'=> $discountprice,
            'PAYMENTREQUEST_0_CURRENCYCODE'=>'USD',
            
        )));
          
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $expcheck = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $expcheck[$name] = urldecode($matches['value'][$offset]);
            }
        }
            
            date_default_timezone_set("UTC");
            
            $date=date('Y-m-d H:i:s', strtotime("+1 Year"));
            $curl = curl_init();
              
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',                          
                'METHOD' => 'CreateRecurringPaymentsProfile',
                'VERSION' => '108',
                             
                'TOKEN' => Input::get('token'),
                'PayerID' => Input::get('PayerID'),
              
                'PROFILESTARTDATE' => $date,
                'DESC' => 'Photo Booth Directory Membership',
                'BILLINGPERIOD' => 'Year',
                'BILLINGFREQUENCY' => '1',
                'AMT' => $saveprice,
                'CURRENCYCODE' => 'USD',
                'COUNTRYCODE' => 'US',
                'MAXFAILEDPAYMENTS' => 1
            )));
              
            $response =    curl_exec($curl);
              
            curl_close($curl);
              
            $profilecheck = array();
              
            if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                foreach ($matches['name'] as $offset => $name) {
                    $profilecheck[$name] = urldecode($matches['value'][$offset]);
                }
            }
            
          
            
                
                
                $company=Company::find(Auth::user()->company->id);
                $company->membership     =  $membership;
                $company->expirationdate =  $date2;
                $company->lastpayment    =  $discountprice;
                $company->payerid        =  Input::get('PayerID');
                $company->profileid      =  $profilecheck['PROFILEID'];
                $company->status         =  'Active';
                $company->save();
                 $emessage=Dj_email_templates::find(2);
          $cmessage=Helper::change_message_variables($emessage->message,'Paypal transaction');
          $sent = \Mail::send('email.email', ['cmessage'=>$cmessage], function($message) use($emessage) {
                
            $message->to(Auth::user()->email, Auth::user()->name)
               ->subject($emessage->subject)
                ->from('manager@thephotoboothdirectory.com',"Photo Booth Admin"); });
                $normaluser=Auth::user();
                $adminuser=Auth::user('admin');

                if(count($normaluser)>0 && count($adminuser)==0){
                    $UserAction                     =       new UserAction;
                    $UserAction->user_id            =       Auth::user()->id;
                    $UserAction->actionidentifier   =       6;
                    $UserAction->save();
                    $UserAction2                     =       new UserAction;
                    $UserAction2->user_id            =       Auth::user()->id;
                    $UserAction2->actionidentifier   =       7;
                    $UserAction2->save();
                }
                Session::flash('alert-success', 'Your Membership Plan Successfully Upgraded');
                return redirect('home');
              
            
        }
      
        $user = Auth::user();
        $pricing=Pricing::find(1);
        $dayLeft=0;
        $credit=0;
       
        if(count(Auth::user()->company)>0){
           if(Auth::user()->company->profileid!=''){
            $company=Auth::user()->company;
            $curl = curl_init();
             curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
                'USER' => 'PayPal_api1.DiscJockeys.com',
                'PWD' => 'HUWQKD926WUVK5A8',
                'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
                  
                  
                    'METHOD' => 'GetRecurringPaymentsProfileDetails',
                    'VERSION' => '108',
                    
                  
                    
                    'PROFILEID' => $company->profileid,
                  
                    
                )));
                  
                $response =    curl_exec($curl);
                  
                curl_close($curl);
                  
                $profilecheck = array();
                  
                if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                    foreach ($matches['name'] as $offset => $name) {
                        $profilecheck[$name] = urldecode($matches['value'][$offset]);
                    }
                }
                // echo "<pre>";
                // print_r($profilecheck);
                // die();
           $profilepaystatus=$profilecheck['STATUS'];
        }
           if(Auth::user()->company->membership>1 && Auth::user()->company->status=='Review' && Auth::user()->company->lastpayment=='')
                return redirect('pay');
            $date=date_create(Auth::user()->company->expirationdate);
            //date_add($date,date_interval_create_from_date_string("1 year"));
            $expiredate=date_format($date,"Y-m-d");
            $date1=date_create($expiredate);
            $date2=date_create("today");
            $diff=date_diff($date2,$date1);
            $dayLeft=$diff->format("%a days");
            $dayLeft2=$diff->format("%a");
            $lastpaymentpaid= (Auth::user()->company->lastpayment!= '' ? Auth::user()->company->lastpayment : 0);
            $credit= ($lastpaymentpaid/365)*$dayLeft2;
            
            if(Auth::user()->company->expirationdate!='')
            $expiredate=date_format($date,"m-d-Y");
            else
             $expiredate="None";
        }
        else{
            return redirect('company');
        }
        return view('home',compact('user','credit','dayLeft','expiredate','profilepaystatus'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
