<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\PriceQuote;
use App\QuoteQueue;
use App\Membership;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PriceQuoteUserContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $PriceQuoteUser=PriceQuote::find($id);
         //echo "<pre>";
         // print_r($PriceQuoteUser->QuoteQueue);
         // foreach ($PriceQuoteUser->QuoteQueue as $QuoteQueue) {
         //    echo $QuoteQueue->user->name;echo "<br>";
         // }
         return view('admin.pricequoteuser.index',compact('PriceQuoteUser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
