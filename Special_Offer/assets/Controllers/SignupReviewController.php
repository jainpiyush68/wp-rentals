<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pricing;
use Auth;
use Session;
use App\Company;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserAction;

class SignupReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $pricing = Pricing::find(1);
        $date2=date_create("today");
        date_add($date2,date_interval_create_from_date_string("1 year"));
         if(Auth::user()->company->membership==1):
           $discountprice=$pricing['pr_free'];
           $saveprice=$pricing['pr_free'];
        elseif(Auth::user()->company->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic'];
        elseif(Auth::user()->company->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze'];
        elseif(Auth::user()->company->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver'];
        elseif(Auth::user()->company->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold'];
        elseif(Auth::user()->company->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum'];
        endif;
      if(Input::get('token')){
       
        $curl = curl_init();
  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'DoExpressCheckoutPayment',
            'VERSION' => '93 ',
          
           
           
          
            'TOKEN' => Input::get('token'),

            'PAYERID'=> Input::get('PayerID'),
            'PAYMENTREQUEST_0_PAYMENTACTION'=>'SALE',
            'PAYMENTREQUEST_0_AMT'=> $discountprice,
            'PAYMENTREQUEST_0_CURRENCYCODE'=>'USD',
            
        )));
          
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $expcheck = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $expcheck[$name] = urldecode($matches['value'][$offset]);
            }
        }
   
        date_default_timezone_set("UTC");
       
        $date=date('Y-m-d H:i:s', strtotime("+1 Year"));
        $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
          
            'METHOD' => 'CreateRecurringPaymentsProfile',
            'VERSION' => '108',
            
          
            'TOKEN' => Input::get('token'),
            'PayerID' => Input::get('PayerID'),
          
            'PROFILESTARTDATE' => $date,
            'DESC' => 'Photo Booth Directory Membership',
            'BILLINGPERIOD' => 'Year',
            'BILLINGFREQUENCY' => '1',
            'AMT' => $saveprice,
            'CURRENCYCODE' => 'USD',
            'COUNTRYCODE' => 'US',
            'MAXFAILEDPAYMENTS' => 1
        )));
          
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $profilecheck = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $profilecheck[$name] = urldecode($matches['value'][$offset]);
            }
        }
         
            $normaluser=Auth::user();
            $adminuser=Auth::user('admin');
             if(count($normaluser)>0 && count($adminuser)==0){
                    $UserAction                     =       new UserAction;
                    $UserAction->user_id            =       Auth::user()->id;
                    $UserAction->actionidentifier   =       7;
                    $UserAction->save();
                    
                }
   
            $company=Company::find(Auth::user()->company->id);
            $company->expirationdate =  $date;
            $company->lastpayment    =  $discountprice;
            $company->payerid        =  Input::get('PayerID');
            $company->profileid      =  $profilecheck['PROFILEID'];
            //$company->status         =  'Active';
            $company->save();
            $sent = \Mail::send('email.reciept', ['paymenttype'=>'Paypal transaction'], function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                ->subject('Welcome to ThePhotoBoothDirectory.com')
                ->from('manager@thephotoboothdirectory.com',"Photo Booth Admin"); });
 }
         $user = Auth::user()->company;
        return view('signupReview.index',compact('user')); 
        }
      
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        
        $company = Auth::user()->company;

        $realName=$request->file('logo');

        if(!empty($realName)){

            $rand=rand();

            $imageName = $rand.'.'.$request->file('logo')->getClientOriginalExtension();
           
            $request->file('logo')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName);

            $company->logo           =   $imageName;

        }
       

       
       
      
        $company->description    =   $request->description;
      
        
       

       

        $company->website_url    =   $request->website_url;
        $company->tagline        =   $request->tagline;
        $company->save();

        //Session::flash('alert-success', 'Thanks for Updating company Infomation');
        return redirect('metro?signup=metro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}