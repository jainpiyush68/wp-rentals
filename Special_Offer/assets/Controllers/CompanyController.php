<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\Pricing;
use App\User;
use Auth;

use Response;
use App\Canadazipcodes;
use App\Uszipcodes;
use Input;
use Omnipay\Omnipay;
use Omnipay\CreditCard;
use Session;
use Payum\Core\Request\GetHumanStatus;
use Payum\LaravelPackage\Controller\PayumController;
use Payum\Paypal\ExpressCheckout\Nvp\Api;
use Payum\Core\Request\Sync;
use Payum\Paypal\ExpressCheckout\Nvp\Request\Api\CreateRecurringPaymentProfile;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        if(count(Auth::user()->company)>0){
            if(Auth::user()->company->membership>1 && Auth::user()->company->status=='Review' && Auth::user()->company->lastpayment=='')
                return redirect('pay');
        }
        $user = Auth::user()->company;
        $pricing = Pricing::find(1);
        return view('company.index',compact('user','pricing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
     

        $pricing = Pricing::find(1);
        $phone=implode('-',$request->phone);
       
        $id=Auth::user()->id;
        // $user = User::find($id);
        // $user->city       =     $request->city;
        // $user->state      =     $request->state;
        // $user->country    =     $request->country;
        // $user->zip        =     $request->zip;
        // $user->address    =     $request->address;
        // $user->save();
         if($request->usstate!=''){
           $state= $request->usstate;
        }
        else{
            $state= $request->castate;
        }
        $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
         if($request->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['sn_free'];
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif($request->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif($request->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif($request->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif($request->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif($request->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;


       if(count(Auth::user()->company)>0){
            $company=Company::find(Auth::user()->company->id);
        }
        else{
            $company = new company;
        }
        $company->address        =   $request->caddress;
        $company->city           =   $request->ccity;
        $company->state          =   $state;
        $company->country        =   $request->ccountry;
        $company->zip            =   $request->czip;
        $company->phone          =   $phone;
        $company->website_url    =   $request->website_url;
        $company->membership     =   $request->membership;
        $company->email          =   $request->email;
        $company->name           =   $request->name;
        $company->userid         =   Auth::user()->id;
        
        $company->status         =  'Review';
        $company->save();
        //$request->session()->flash('alert-success', 'Your Membership Plan Successfully Added');
       

      if($request->membership==1)
           return redirect('signupreview');
          else
            return redirect('paypalpay');
        
        $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '93',
           
            
             'PAYMENTREQUEST_0_AMT' =>$discountprice,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => $discountprice,
          
            'L_PAYMENTREQUEST_0_NAME0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_DESC0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_QTY0' => 1,
            'L_PAYMENTREQUEST_0_AMT0' => $discountprice,
            'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Physical',
           
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => 'Photo Booth Directory Membership',
          
            'CANCELURL' => url('pay'),
            'RETURNURL' => url('signupreview'),
            'NOTIFYURL' => url('signupreview'),

        )));
         
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $nvp = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $nvp[$name] = urldecode($matches['value'][$offset]);
            }
        }
         print_r($nvp);
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $query = array(
                'cmd'    => '_express-checkout',
                'token'  => $nvp['TOKEN']
            );
         
            $redirectURL = sprintf('https://www.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
         
            header('Location: ' . $redirectURL);
        } else {
            //Opz, alguma coisa deu errada.
            //Verifique os logs de erro para depuração.
        } 


            
           
    die();
        //return redirect('signupreview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function city(Request $request){

        $States=Canadazipcodes::where('State', '=',  $request->stateid)->groupBy('City')->get();
            
        if($States->count()==0)

            $States=Uszipcodes::where('State', '=',  $request->stateid)->groupBy('City')->get();

        return Response::view('ajax.cityajax',compact('States'));

    }
}