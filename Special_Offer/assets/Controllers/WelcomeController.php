<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cms;
use App\Articles;
use App\Company;
use App\Banner;
use App\States;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages=Cms::all();
        $States=States::all();
        $articles=Articles::orderBy('priority','ASC')->orderBy('updated_at','DESC')->get();
        return view('welcome',compact('pages','States','articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function checkpayment(){
        $companies=Company::where('status','=','Active')->get();
            foreach ($companies as $key => $company) {
                if($company->membership>1 && $company->profileid!=''){
                    $curl = curl_init();
                  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($curl, CURLOPT_POST, true);
                  curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
                  curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
                  'USER' => 'PayPal_api1.DiscJockeys.com',
                  'PWD' => 'HUWQKD926WUVK5A8',
                  'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
                  
                  
                    'METHOD' => 'GetRecurringPaymentsProfileDetails',
                    'VERSION' => '108',
                    
                  
                    
                    'PROFILEID' => $company->profileid,
                  
                    
                )));
                  
                $response =    curl_exec($curl);
                  
                curl_close($curl);
                  
                $profilecheck = array();
                  
                if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                    foreach ($matches['name'] as $offset => $name) {
                        $profilecheck[$name] = urldecode($matches['value'][$offset]);
                    }
                }
                $date=date_create($company->expirationdate);
                //date_add($date,date_interval_create_from_date_string("1 year"));
                $expiredate=date_format($date,"Y-m-d");
                $date1=date_create($expiredate);
                $date2=date_create("today");
                $diff=date_diff($date2,$date1);
                $dayLeft=$diff->format("%R%a days");
                if($dayLeft<=0)
                {
                    if($profilecheck['STATUS']=='Active'){
                        
                        $date2=date_create("today");
                        date_add($date2,date_interval_create_from_date_string("1 year"));
                        $company=Company::find($company->id);
                       
                        $company->expirationdate =  $date2;
                        $company->lastpayment    =  $profilecheck['AMT'];
                        
                        $company->status         =  'Active';
                        $company->save();
                        
                      
                    }
                    elseif($profilecheck['STATUS']=='Suspended' || $profilecheck['STATUS']=='Cancelled'){
                        
                        
                        $company=Company::find($company->id);
                        $company->membership     =  1;
                        $company->expirationdate =  '';
                        $company->lastpayment    =  '';
                        
                        $company->status         =  'Active';
                        $company->save();
                        if(count($company->user->banner)>0){

                            $Banner=Banner::find($company->user->banner->id);
                            $Banner->status     =   'Inactive';
                            $Banner->save();
                            
                        }
                       
                      
                    }
                    //echo "success";
                }
              
                //die;
            }
        }
       
    }
}
