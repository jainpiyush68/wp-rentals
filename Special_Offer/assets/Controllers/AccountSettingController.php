<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use Validator;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('account.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user = Auth::user();
        if (!\Hash::check(\Input::get('oldpass'), $user->password)) {
               
                Session::flash('alert-success', 'Your old password does not match');

                return redirect('account');

        }
        $rules = array(
            
            'pass' => 'min:6',
            'cpass' => 'same:pass'
        );
         $messages = [
            'same'    => 'The Confirm New password and New password must match..',
            'min'    => 'The Password must be at least 6 characters..'
    
        ];

        $validator = \Validator::make(Input::all(), $rules,$messages);
       
        $validator->after(function($validator)
        {
            if ($this->checkCurrentPassword(\Input::get('pass')))
            {
                $validator->errors()->add('pass', 'Please enter password other than current password!');
            }
        });

        if ($validator->fails()) {
            return \Redirect::to('account')->withErrors($validator);
        } else {

            if (!\Hash::check(\Input::get('oldpass'), $user->password)) {
               
                Session::flash('alert-success', 'Your old password does not match');

                return redirect('account');

            } else {
                if($request->pass==''){
                    $user->email    = $request->email;
                    $user->save();
                }
                else{
                $user->email    = $request->email;
                $user->password = bcrypt(Input::get('pass'));
                $user->save();
             }
            //Session::flash('alert-success', 'Your login settings were saved successfully');

            return redirect('home');
            }
        }
    }
    public function checkCurrentPassword($password){
        if (Auth::attempt(array('email' => Auth::user()->email, 'password' => $password))){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
