<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Services;
use App\UserServices;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $services=Services::where('type','=','service')->orderBy('sort_by', 'Asc')->get();
        $events=Services::where('type','=','event')->orderBy('services', 'Asc')->get();
        return view('services.index',compact('services','events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $services=implode(',', $request->event);
        $UserServices= New  UserServices;
        $UserServices->companyid =Auth::user()->company->id;
        $UserServices->services =$services;
        $UserServices->save();
        
        if($request->services=='services'){
            return redirect('metro?signup=metro');
        }
        else{
           // Session::flash('alert-success', 'Your company services and events were saved successfully');
            return redirect('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $services=implode(',', $request->event);
        $UserServices= UserServices::find($id);
        
        $UserServices->services =$services;
        $UserServices->save();
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
