<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use App\Gallery;
use Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Helper;
use App\Dj_email_templates;

class SignupGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::flash('signupconfirm', 'Thanks for fillup company Information');
        $galleries = Auth::user()->company->gallery()->orderBy('gallery_id', 'DESC')->get();
        $user = Auth::user()->company;
        return view('signupGallery.index',compact('galleries','user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $company = Auth::user()->company;

          
        $rand=rand();

        $gallery = new Gallery;
         if($request->order!=''){
          $gallery->priority        =   $request->order;
         $increase=$request->order +1;
        $priorities=Gallery::where('priority','=', $request->order)->where('company_id','=', $company->id)->get();
        foreach($priorities as $priority){
         $changepriority=Gallery::find($priority->gallery_id);
         $changepriority->priority  =$increase;
         $changepriority->save();

         }
        } 

        $imageName = $rand.'.'.$request->file('photo')->getClientOriginalExtension();
       
        $check=filesize($request->file('photo'));
                $check=($check/(1024*1024));
        if($check<2){
            $request->file('photo')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName);
        }
        else{

            $test=getimagesize($request->file('photo'));
            $width=$test[0]*.5;
            $height=$test[1]*.5;
            $image =$request->file('photo');
            $path = public_path('assets/gallery/'.$company->id.'/'.$imageName);
            Image::make($image->getRealPath())->resize($width, $height)->save($path);

        }

        $gallery->image        =   $imageName;

        $gallery->caption      =   $request->caption;

        $gallery->company_id   =   $company->id;

        $gallery->save();
        if(Auth::user()->company->membership<5){
           // Session::flash('alert-success', 'Thanks for fillup company Information');
           // return redirect('home');  
        }
        else{
            
         //  return redirect('video?signup=video');   
        }
      $emessage=Dj_email_templates::find(1);
          $cmessage=Helper::change_message_variables($emessage->message,'Paypal transaction');
          $sent = \Mail::send('email.email', ['cmessage'=>$cmessage], function($message) use($emessage) {
                
            $message->to(Auth::user()->email, Auth::user()->name)
               ->subject($emessage->subject)
          ->from('management@ThePhotoboothdirectory.com',"Management");
        });
      return redirect('signupgallery'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
