<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;
use App\Feedback;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CompanyReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cid)
    {
        //
        $company_id=explode('-',$cid);

        $size=sizeof($company_id);

        $company=Company::find($company_id[$size-1]);

        return view('companyReview.index',compact('company','cid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $company_id=explode('-',$request->slug);

        $size=sizeof($company_id);
        
        $feedback = new Feedback;

        $feedback->visitorname       =       $request->visitorname;

        $feedback->comment          =       $request->comment;

        $feedback->star_rating      =       $request->rating;

        $feedback->company_id      =       $company_id[$size-1];

        $feedback->save();

        $request->session()->flash('alert-success', 'Your review was submitted');

        return redirect('profile/'.$request->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $Feedback=Feedback::find($id);
        $slug=$Feedback->company->name.'-'.$Feedback->company->id;
        $Feedback->delete();
        return redirect('feedback/'.$slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
