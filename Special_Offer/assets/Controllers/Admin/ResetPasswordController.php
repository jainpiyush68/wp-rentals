<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Input;
use Validator;
use Session;
use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ResetPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $Company=Company::find($id);
        return view('admin.resetpassword.index',compact('Company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          $company = Company::find($id);
        $user=$company->user;
        $rules = array(
            
            'pass' => 'min:6|required',
            
        );
         $messages = [
            'required'=>'Password is Required',
           
            'min'    => 'The Password must be at least 6 characters..'
    
        ];

        $validator = \Validator::make(Input::all(), $rules,$messages);


        if ($validator->fails()) {
            return \Redirect::to('admin/reset/'.$id)->withErrors($validator);
        } else {

           
                
               
                
                $user->password = bcrypt(Input::get('pass'));
                $user->save();
             
            //Session::flash('alert-success', 'Your Account Information was uploaded Update');

            return redirect('admin/reset/'.$id);
            
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
