<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pricing;
use Auth;
use App\Company;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserAction;

class PayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         if(Input::get('token')){
            $normaluser=Auth::user();
            $adminuser=Auth::user('admin');
            if(count($normaluser)>0 && count($adminuser)==0){
                    $UserAction                     =       new UserAction;
                    $UserAction->user_id            =       Auth::user()->id;
                    $UserAction->actionidentifier   =       8;
                    $UserAction->save();
                    
            }
        }
         $pricing = Pricing::find(1);
        return view('pay.index',compact('pricing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company=Company::find(Auth::user()->company->id);
        $company->membership     =   $request->membership;
        $company->save();
         if($request->membership==1)
          return redirect('signupreview');
         $pricing = Pricing::find(1);

        if(Auth::user()->company->status=='Review')
        {

           $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
         if($request->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['sn_free'];
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif($request->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif($request->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif($request->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif($request->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif($request->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;
        $returnUrl=url('signupreview');
        }
        else{
             if(Auth::user()->company->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['up_free'];
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif(Auth::user()->company->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['up_basic'];
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif(Auth::user()->company->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['up_bronze'];
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif(Auth::user()->company->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['up_silver'];
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif(Auth::user()->company->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['up_gold'];
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif(Auth::user()->company->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['up_platinum'];
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;
        $returnUrl=url('home');

        }
        echo $discountprice;
        $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '108',
           
            
            'PAYMENTREQUEST_0_AMT' =>$discountprice,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => $discountprice,
          
            'L_PAYMENTREQUEST_0_NAME0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_DESC0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_QTY0' => 1,
            'L_PAYMENTREQUEST_0_AMT0' => $discountprice,
            'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Physical',
          
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => 'Photo Booth Directory Membership',
          
            'CANCELURL' => url('pay'),
            'RETURNURL' => $returnUrl,
            

        )));
         
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $nvp = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {

                $nvp[$name] = urldecode($matches['value'][$offset]);
            }
        }
        
         
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $query = array(
                'cmd'    => '_express-checkout',
                'token'  => $nvp['TOKEN']
            );
             
             $redirectURL = sprintf('https://www.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
         
            header('Location: ' . $redirectURL);
        } else {
            //Opz, alguma coisa deu errada.
            //Verifique os logs de erro para depuração.
        } 
        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
             $status=$request->payprofilestatus;
          $curl = curl_init();
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
          curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
              
              
                'METHOD' => 'ManageRecurringPaymentsProfileStatus',
                'VERSION' => '108',
                
              
                
                'PROFILEID' => Auth::user()->company->profileid,
              
                'ACTION' => $status
            )));
              
            $response =    curl_exec($curl);
              
            curl_close($curl);
              
            $cancelcheck = array();
              
            if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                foreach ($matches['name'] as $offset => $name) {
                    $cancelcheck [$name] = urldecode($matches['value'][$offset]);
                }
            }
       /* $Company=Company::find($id);
        $Company->membership = 1;
        $Company->Save();*/
        return redirect('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
