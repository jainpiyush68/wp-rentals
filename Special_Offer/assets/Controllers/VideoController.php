<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use App\Video;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\UserAction;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Input::get('signup')=='video')
           Session::flash('signupconfirm', '');
        
        //$videos = Auth::user()->company->video;
        $user = Auth::user()->company;
        return view('video.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company = Auth::user()->company;

        $youtube_id=explode('=',$request->youtube_url);

        $video=new Video;

        $video->video_name      =   $request->name;

        $video->youtube_id      =   $youtube_id[1];

        $video->company_id      =   $company->id;

        $video->save();
        if($request->signup=='video'){
           return redirect('video?signup=video');
        }
        else{
            $normaluser=Auth::user();
            $adminuser=Auth::user('admin');

            if(count($normaluser)>0 && count($adminuser)==0){
                $UserAction                     =       new UserAction;
                $UserAction->user_id            =       Auth::user()->id;
                $UserAction->actionidentifier   =       5;
                $UserAction->save();
            }
        }
        Session::flash('alert-success', 'Your video gallery was saved successfully');
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Video=Video::find($id);
        $Video->delete();

        return redirect('video');
    }
}
