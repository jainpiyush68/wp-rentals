<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
use Session;
use App\Banner;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banners=Banner::where('status','=','Review')->orderBy('id','DESC')->get();;
        return view('admin.banner.index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $banner=Banner::find($request->banner_id);

        $banner->status     =   'Active';
        $banner->save();

        //Session::flash('alert-success', 'Banner was Activated . ');
        return redirect('admin/banner');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $banner=Banner::find($id);
        return view('admin.banner.show',compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Banner = Banner::find($id);
        $Banner->time      =     $request->time;
        $Banner->status     =   'Active';
        $Banner->save();
        //Session::flash('alert-success', 'Banner Was Updated');
         if($request->status=="Review")
           return redirect('admin/banner');
       else
         return redirect('admin/activebanner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Banner = Banner::find($id);
        $Banner->delete();
        //Session::flash('alert-success', 'Banner  Was Deleted');
        return redirect('admin/banner');
    
    }
public function active()
    {
        //
        $banners=Banner::where('status','=','Active')->get();
        return view('admin.banner.active',compact('banners'));
    }
}