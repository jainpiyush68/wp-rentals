<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Blog;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs=Blog::orderBy('id', 'DESC')->get();
        return view('admin.blog',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $Blog = new Blog;
        $Blog->title          =   $request->title;
        $Blog->body           =   $request->body;
        $Blog->author         =   $request->author;
        $Blog->seotitle       =   $request->seotitle;
        $Blog->seodescription =   $request->seodescription;
        $Blog->seokeywords    =   $request->seokeywords;
        $Blog->save();
        //Session::flash('alert-success', 'Your Blog Successfully Added');
        return redirect('admin\blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $allblogs=Blog::orderBy('id', 'DESC')->get();
        $Blog = Blog::find($id);
        return view('admin.editblog',compact('Blog','allblogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Blog = Blog::find($id);
        $Blog->title      =     $request->title;
        $Blog->body       =     $request->body;
        $Blog->author     =     $request->author;
        $Blog->seotitle       =   $request->seotitle;
        $Blog->seodescription =   $request->seodescription;
        $Blog->seokeywords    =   $request->seokeywords;
        $Blog->save();
        //Session::flash('alert-success', 'Your Blog Successfully Edit');
        return redirect('admin/blog/edit/'.$id);
    


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Blog = Blog::find($id);

        $Blog->delete();

        return redirect('admin\blog');
    }
}
