<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rentals');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c[r~0%gCCg.2B/&nc<:=r~qA(&G!am>B4c6w<lPHf25%yirX1[xx,Y2Hi(R%lk[%');
define('SECURE_AUTH_KEY',  'RdBz^,%sM?z#,WrrCpSfo0xzL~{UKDFw3pg4s>0<F:Iyx~P,mAceyO;PhWqA2uaV');
define('LOGGED_IN_KEY',    ':}FZyq+,(YV`tRz:~EC)5Xt|$K[U9Khpc_k<J@x;$7:cg@kt8Ha;=(C3iWx%1Tyu');
define('NONCE_KEY',        '~HVoNMcGl*?L5&I}Z}^gnfBL-p,3fRX[j=9nM^J1n]:7+9o!BTBk-<dNi6Pk|<_3');
define('AUTH_SALT',        '9RLD4,1>koXzOu4gO^!F(c4T6j[P}LD/UbsDQ>x9{?Gv YeY;;ot.:0%[;Km}5 ?');
define('SECURE_AUTH_SALT', 'Rr>Y8TOydZuU8/)QPH<ZN ~d1CVG7WRwAsw>#ZViy($3!TD7yH]B3Ry${fJs9m45');
define('LOGGED_IN_SALT',   '6Beu8>J>e+yhviJR<(lhCyR BGOOc`K!Nti(1}4xf7Cca!`f9+FkbYMPh^z=Y=Dm');
define('NONCE_SALT',       'qiRT%AI=3=iBNpTCf8Ef-&r}QqZY}>,qS+!KN+w3jPlU{cp^Srt]w}o[>LG.HHc ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
