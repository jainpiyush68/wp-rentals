<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Banner;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company = Auth::user()->company;
        
        $rand=rand();

        $Banner = new Banner;

        $imageName = $rand.'.'.$request->file('logo')->getClientOriginalExtension();
       
        $request->file('logo')->move(base_path() . '/public/assets/images/banner/', $imageName);

        $Banner->file_name        =   $imageName;

        $Banner->user             =   Auth::user()->id;

        $Banner->status           =   'Review';

        $Banner->save();

       // Session::flash('alert-success', 'Your banner was uploaded. We will review and approve it soon');
    
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $company = Auth::user()->company;

        $prev=Auth::user()->banner->file_name;   
        $rand=rand();

        $Banner=Banner::find($id);

        $imageName = $rand.'.'.$request->file('logo')->getClientOriginalExtension();
       
        $request->file('logo')->move(base_path() . '/public/assets/images/banner/', $imageName);

        $Banner->file_name        =   $imageName;

        $Banner->user             =   Auth::user()->id;

        $Banner->status           =   'Review';

        $Banner->save();

        //Session::flash('alert-success', 'Banner was updated ');
        unlink(base_path() . '/public/assets/images/banner/'. $prev);
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
