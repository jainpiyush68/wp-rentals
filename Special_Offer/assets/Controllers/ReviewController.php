<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\UserAction;
use App\States;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $user = Auth::user()->company;
         $States=States::where('country','=',Auth::user()->company->country)->get();
        return view('review.index',compact('user','States'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $phone=implode('-',$request->phone);
        
        $company = Auth::user()->company;

        $realName=$request->file('logo');

        if(!empty($realName)){

            $rand=rand();

            $imageName = $rand.'.'.$request->file('logo')->getClientOriginalExtension();
           
            $request->file('logo')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName);

            $company->logo           =   $imageName;

        }
       

        $company->address        =   $request->address;
        $company->city           =   $request->city;
        $company->state          =   $request->state;
       
        $company->description    =   $request->description;
        $company->zip            =   $request->zip;
        $company->phone          =   $phone;
        $company->name           =   $request->name;
        $company->email          =   $request->email;
        if(Auth::user()->company->membership==1)
          $company->status         =  'Review';
       

        $company->website_url    =   $request->website_url;
        $company->tagline        =   $request->tagline;

      


        $company->save();
        $normaluser=Auth::user();
        $adminuser=Auth::user('admin');

        if(count($normaluser)>0 && count($adminuser)==0){
            $UserAction                     =       new UserAction;
            $UserAction->user_id            =       Auth::user()->id;
            $UserAction->actionidentifier   =       1;
            $UserAction->save();
        }

        Session::flash('alert-success', 'Your company information was saved successfully');
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
