<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
//use App\Online;
//use App\Session;
use Tracker;
use App\User;
use App\Company;
use App\TrackerPath;
use App\Trakerlog;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DateTime;
use Carbon\Carbon;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
       
        $visitor = Tracker::users(60 * 2);
       
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',1);})->count();
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',2);})->count();
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',3);})->count();
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',4);})->count();
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',5);})->count();
        $membership[]=User::whereHas('company', function($q){$q->where('membership', '=',6);})->count();
        $membership[]=User::whereHas('company', function($q){$q->whereIn('membership',[1, 2, 3,4,5,6]);})->count();
        
        $loggedin   =   count($visitor);

        $month_ini = new DateTime("first day of last month");
        $month_end = new DateTime("last day of last month");

        $lastto     =    $month_ini->format('Y-m-d'); // 2012-02-01
        $lastfrom   =    $month_end->format('Y-m-d'); // 2012-02-29

        $to=date('Y-m-d H:i:s', strtotime("today"));
        $from=date('Y-m-d H:i:s', strtotime("+1 day"));
        $firstmoth=date('Y-m-d', strtotime(date('Y-m-1')));
        $start = new Carbon('first day of this month');
        $end   = new Carbon('last day of this month');

        $lastMonthLog = Tracker::users($start->diffInMinutes($end))->count();


        $monthsearch    =   Trakerlog::where('path_id','=',1)->whereBetween('created_at', array($firstmoth, $from))->count();
        $searchpage     =   Trakerlog::where('path_id','=',1)->whereBetween('created_at', array($to, $from))->count();
        $lastsearch     =   Trakerlog::where('path_id','=',1)->whereBetween('created_at', array($lastto, $lastfrom))->count();

        $homepage       =   Trakerlog::where('path_id','=',2)->whereBetween('created_at', array($to, $from))->count();
        $monthhome      =   Trakerlog::where('path_id','=',2)->whereBetween('created_at', array($firstmoth, $from))->count();
        $lasthome       =   Trakerlog::where('path_id','=',2)->whereBetween('created_at', array($lastto, $lastfrom))->count();

        
        $startcm = Carbon::now()->startOfMonth();
        $endcm = Carbon::now();
        $today = Carbon::today();
        $todaylogged    =   Tracker::users($today->diffInMinutes($endcm))->count();
        $monthlogged    =   Tracker::users($startcm->diffInMinutes($endcm));
        
        $registertoday  =   Company::whereBetween('created_at', array($to, $from))->count();
        $registermonth  =   User::whereHas('company',function($q) use($firstmoth, $from){$q->whereBetween('created_at', array($firstmoth, $from));})->count();
        $registerlast   =   Company::whereBetween('created_at', array($lastto, $lastto))->count();
        return view('admin.membercount',
                        compact('membership',
                                'loggedin'  ,
                                'homepage',
                                'searchpage',
                                'registertoday',
                                'registermonth',
                                'todaylogged',
                                'monthlogged',
                                'monthsearch',
                                'monthhome',
                                'lastsearch',
                                'lasthome',
                                'registerlast',
                                'lastMonthLog'
                            )
                    );
       
        //return view('admin.membercount',compact('membership','loggedin','homepage','searchpage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
