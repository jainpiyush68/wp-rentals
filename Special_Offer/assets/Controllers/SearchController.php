<?php

namespace App\Http\Controllers;
ini_set('memory_limit','-1');
use Illuminate\Http\Request;
use Response;
use URL;
use DB;
use Tracker;
use App\Company;
use App\Canadazipcodes;
use App\Uszipcodes;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cms;
use App\Banner;

class SearchController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $perpage=20;
    public function index(Request $request)
    {
        //
        $banners=array();
        $pages=Cms::all();
        $zip=array();
        $test=array();
        $test2=array();
        $test3=array();
        $city="";
        if(isset($request->city)){
            $companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->city . '%');

                                        })
                                ->where('status','=','Active')
                                ->orWhere(function($query) use($request)
                                    {
                                        
                                        $query->wherehas('us_location',function($query) use($request){
                                        $query->where('City', 'LIKE', '%' . $request->city . '%');

                                    });
                                        
                                    })
                                ->where('status','=','Active')
                                ->orderBy('membership', 'DESC')
                                ->paginate($this->perpage);
        }
        elseif(isset($request->state))
        {
            
             $companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('State', 'LIKE', '%' . $request->state . '%');

                                        })
                                    ->where('status','=','Active')
                                     ->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('us_location',function($query) use($request){
                                            $query->where('State', 'LIKE', '%' . $request->state . '%');

                                        });
                                            
                                        })
                                    ->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')
                                    ->paginate($this->perpage);
        
        }
        else{

            $zip=Canadazipcodes::where('ZIP', '=',  $request->title)->get();
            
            if($zip->count()==0)

                 $zip=Uszipcodes::where('ZIP', '=',  $request->title)->get();

           
            if($zip->count()>0){
               
                $banners=Banner::wherehas('userInfo',
                                            function($query) use($zip){
                                                $query->wherehas('company',
                                                    function($query) use($zip){
                                                      $query->wherehas('metro',
                                                             function($query) use($zip){
                                                                $query->where('city', 'LIKE', '%' .$zip[0]->City . '%');
                                                             });  
                                                    });

                                            }
                                        )
                                ->where('status','=','Active')->get();
                $lat1 = $zip[0]->Latitude;
                $lon1 = $zip[0]->Longitude;
                $d = 100;
                $r = 3959;

                $latN = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(0))));
                $latS = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(180))));
                $lonE = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(90)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
                $lonW = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(270)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
                
                $query=Canadazipcodes::where('Latitude', '<=',  $latN)
                                        ->where('Latitude', '>=',  $latS)
                                        ->where('Longitude', '>=',  $lonE)
                                        ->where('Longitude', '<=',  $lonW)
                                        ->where('City', '!=',  '')
                                        ->get();
                if($query->count()==0){

                        $query=Uszipcodes::where('Latitude', '<=',  $latN)
                                        ->where('Latitude', '>=',  $latS)
                                        ->where('Longitude', '<=',  $lonE)
                                        ->where('Longitude', '>=',  $lonW)
                                        ->where('City', '!=',  '')
                                        ->get();

                }
                
                $tmp = array();
                $i = 0;
                
                foreach ($query as $row) {
              
                    $distance = round(acos(sin(deg2rad($lat1)) * sin(deg2rad($row['Latitude'])) + cos(deg2rad($lat1)) * cos(deg2rad($row['Latitude'])) * cos(deg2rad($row['Longitude']) - deg2rad($lon1))) * $r);
                    if(50 >= $distance) {
                        $tmp[$i] = $row;
                        $test[]=$row['ZIP'];
                        $tmp[$i]['distance'] = $distance;
                        $i++;
                    }
                    if(75 >= $distance) {
                        $tmp[$i] = $row;
                        $test2[]=$row['ZIP'];
                        $tmp[$i]['distance'] = $distance;
                        $i++;
                    }
                    if(100>= $distance) {
                        $tmp[$i] = $row;
                        $test3[]=$row['ZIP'];
                        $tmp[$i]['distance'] = $distance;
                        $i++;
                    }
                }
                                        
                
                $size=sizeof($test);                  
               $zips=implode(', ',$test);
                                        
              
           }                     
            $companies=Company::where('name', 'LIKE', '%' . $request->title . '%')
                                    ->where('status','=','Active')
                                    ->orWhere(function($query) use($test)
                                        {
                                            
                                            $query->whereIn('zip',$test);
                                            
                                        })
                                    ->where('status','=','Active')
                                    ->orWhere(function($query) use($zip)
                                        {
                                            if(count($zip)!=0){
                                            $query->wherehas('metro',function($query) use($zip){
                                            $query->where('city', 'LIKE', '%' . $zip[0]->City . '%');
                                            

                                        });
                                         }   
                                        })
                                    ->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->paginate($this->perpage);

            if($companies->count()==0)
            {
                   $companies=Company::where('city', 'LIKE', '%' . $request->title . '%')
                                    ->where('status','=','Active')
                                     
                                    ->orWhere(function($query) use($zip)
                                        {
                                            if(count($zip)!=0){
                                            $query->wherehas('metro',function($query) use($zip){
                                            $query->where('city', 'LIKE', '%' . $zip[0]->City . '%');
                                            

                                        });
                                         }   
                                        })
                                    ->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->paginate($this->perpage);
                  /*$companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->title . '%');

                                        })
                                    ->where('status','=','Active')
                                     ->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('us_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->title . '%');

                                        });
                                            
                                        })
                                    ->where('status','=','Active')
                                    ->orWhere(function($query) use($zip)
                                        {
                                            if(count($zip)!=0){
                                            $query->wherehas('metro',function($query) use($zip){
                                            $query->where('city', 'LIKE', '%' . $zip[0]->City . '%');
                                            

                                        });
                                         }   
                                        })
                                    ->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')
                                    ->paginate($this->perpage);*/
                 $city="citysearch";
            }
        }
                                
                                
        $paginate=1;
        $perpage=$this->perpage;
        $parsezip=implode(',', $test);
        $parsezip2=implode(',', $test2); 
        $parsezip3=implode(',', $test3);                  
        return view('search.index',compact('companies','zip','paginate','perpage','parsezip','parsezip2','parsezip3','city','pages','banners'));

    
    }

    public function ajax(Request $request){
       
      
        $page=$request->page*$this->perpage;

        if($request) {
                            
            $test=explode(',', $request->zip);
                               
            $companies=Company::whereIn('zip',$test)->where('status','=','Active')->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('metro',function($query) use($request){
                                            $query->where('city', 'LIKE', '%' . $request->mcity . '%');

                                        });
                                            
                                        })
                                    ->where('status','=','Active')->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))->paginate($this->perpage);
           
            $paginate=1;
            $perpage=$this->perpage;
            return Response::view('ajax.index',compact('companies','zip','paginate','perpage'));
        } else {
         $response = array(
                'status' => 'success Http',
                'msg' => 'Setting created successfully',
            );

            return ($response);
        }
    }
    public function readmore(Request $request){

       
        $page=$request->page*$this->perpage;
        $test=array();
        if($request) {
             
                $perpage=$this->perpage;
                if($request->zip!='')                           
                $test=explode(',', $request->zip);
                $paginate=0; 
            if(isset($request->title)){
                     $companies=Company::where('name', 'LIKE', '%' . $request->title . '%')
                                    ->where('status','=','Active')
                                   
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();

            if($companies->count()==0)
            {
                   $companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->city . '%');

                                        })->where('status','=','Active')
                                    ->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('us_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->city . '%');

                                        });
                                    })->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();
                 $city="citysearch";
            }
            }             
                
            elseif(isset($request->city) && $request->city!='')

                 $companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->city . '%');

                                        })->where('status','=','Active')
                                    ->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('us_location',function($query) use($request){
                                            $query->where('City', 'LIKE', '%' . $request->city . '%');

                                        });
                                    })->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();
            elseif(isset($request->state) && $request->state!='')

                 $companies=Company::wherehas('ca_zip_location',function($query) use($request){
                                            $query->where('State', 'LIKE', '%' . $request->state . '%');

                                        })->where('status','=','Active')
                                    ->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('us_location',function($query) use($request){
                                            $query->where('State', 'LIKE', '%' . $request->state . '%');

                                        });
                                    })->where('status','=','Active')
                                    ->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();
            elseif(count($test)>0){
            $companies=Company::whereIn('zip',$test)->where('status','=','Active')->orWhere(function($query) use($request)
                                        {
                                            
                                            $query->wherehas('metro',function($query) use($request){
                                            $query->where('city', 'LIKE', '%' . $request->mcity . '%');

                                        });
                                            
                                        })
                                    ->where('status','=','Active')->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();
            }
            else{
             $companies=Company::where('status','=','Active')->orderBy('membership', 'DESC')->orderBy(DB::raw('RAND()'))
                                    ->take($this->perpage)
                                    ->skip($page)
                                    ->get();

         }
            
            return Response::view('ajax.index',compact('companies','zip','paginate','perpage'));
        } else {
         $response = array(
                'status' => 'success Http',
                'msg' => 'Setting created successfully',
            );

            return ($response);
        }

    }
    public function video(Request $request){
        $cat_id=explode('_', $request->cat);
        $companies=Company::where('id','=',$cat_id[1])->get();
        $cname=$companies[0]->name;
        $gallery=$companies[0]->gallery->count();
        $video=$companies[0]->video;
        return \Response::json(compact('video','cname','gallery'));
    }
    public function gallery(Request $request){
        $cat_id=explode('_', $request->cat);
        $companies=Company::where('id','=',$cat_id[1])->get();
        $cname=$companies[0]->name;
        $gallery=$companies[0]->gallery()->orderBy('priority', 'ASC')->get();
        $video=$companies[0]->video->count();
        return \Response::json(compact('gallery','cname','video'));
    }
    public function quote(Request $request){
        $cat_id=explode('_', $request->cid);
        $companies=Company::find($cat_id);
        $phone=$request->phone1.'-'.$request->phone2.'-'.$request->phone3;
        $sent = \Mail::send('email.searchquote',['name'=>$request->uname,'email'=>$request->email,'phone'=>  $phone,'edate'=>$request->edate,'etype'=>$request->etype,'zip'=>$request->zip,'day'=>$request->day,'time'=>$request->time,'guest'=>$request->guest,'budget'=>$request->budget,'detail'=>str_replace("\n","<br/>",$request->detail)], function($message) use($companies,$request){
            $message->to('piyush@whitebrains.in', $companies[0]->name)
                ->subject('Price Quote Lead!')
                ->from('quote-leads@ThePhotoBoothDirectory.com');
           $message->getHeaders()->addTextHeader('Return-Path', 'do-not-reply@ThePhotoBoothDirectory.com');
        });
        if($sent){
            $response = array(
                'msg' => 'success',
            );
        }
        else{
            $response = array(
                'msg' => 'fail',
            );

        }
        return \Response::json(compact('response'));
    }
   
}
