<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
/*use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;*/
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use App\Http\Requests\Auth\LoginRequest; 
use App\Http\Requests\Auth\RegisterRequest;
class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectAfterLogout = "admin";

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->user = "admin";
        $this->auth = $auth->with('admin');
        $this->middleware('admin.guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function getAdminLogin() {
        return View('admin.index');
    }

    protected function postLogin(LoginRequest $request) {

        $condition = Admin::where('email', '=', $request->email)
                    
                    ->exists();

        if($condition){
            if (Auth::attempt("admin",$request->only('email', 'password'))) {
                
              

                 

                    return redirect('/admin/home');//redirect()->route('course');
                
            }
        }
 
        return redirect('admin')->withErrors([
            'email' => 'The email or the password is invalid. Please try again.',
        ]);
    }
     protected function getLogout()
    {
        

        

        $this->auth->logout();
       
        return redirect('admin');
    }
}
