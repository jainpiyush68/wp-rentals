-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2017 at 06:08 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentals`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_tenant_questions_answer`
--

CREATE TABLE `wp_tenant_questions_answer` (
  `ID` int(11) NOT NULL,
  `answer` text NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_tenant_questions_answer`
--

INSERT INTO `wp_tenant_questions_answer` (`ID`, `answer`, `question_id`, `value`) VALUES
(1, 'Male', 3, 'male'),
(2, 'Female', 3, 'female'),
(3, 'Whatsapp ', 9, 'whatsapp'),
(4, 'Whatsapp Call', 9, 'whatsappcall'),
(5, 'Video Call', 9, 'videocall'),
(6, 'E-Mail', 9, 'email'),
(7, 'Facebook', 21, 'facebook'),
(8, 'LinkedIn', 21, 'linkedln');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_tenant_questions_answer`
--
ALTER TABLE `wp_tenant_questions_answer`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_tenant_questions_answer`
--
ALTER TABLE `wp_tenant_questions_answer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
