<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use App\Admin;
use Validator;
use Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
/*use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;*/
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Auth\LoginRequest; 
use App\Http\Requests\Auth\RegisterRequest;
use ReCaptcha\ReCaptcha;
use DB;
use Session;
use App\Cms;
use App\UserAction;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * User model instance
     * @var User
    */
   // protected $user;

    /**
     * For Guard
     *
     * @var Authenticator
    */
    //protected $auth;



    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user; 
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'last' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    public function captchaCheck()
    {

        $response = Input::get('g-recaptcha-response');
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret   = env('RE_CAP_SECRET');

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /* Login get post methods */
    protected function getLogin() {
         $pages=Cms::all();
        return View('users.login',compact('pages'))->with('title','Members System - Login');
    }
    protected function getAdminLogin() {
        return View('admin.index');
    }

    protected function postLogin(LoginRequest $request) {

        $condition = User::where('email', '=', $request->email)
                    
                    ->exists();

        if($condition){
            if (Auth::attempt("user",$request->only('email', 'password'))) {
                
                    $logintime=date('Y-m-d H:i:s');
                    $normaluser=Auth::user();
                    $adminuser=Auth::user('admin');
                     if(count($normaluser)>0 && count($adminuser)==0){
                            $is_login=Auth::user();
                            $is_login->lastlogin            =       $logintime;
                            $is_login->save();

                            $UserAction                     =       new UserAction;
                            $UserAction->user_id            =       Auth::user()->id;
                            $UserAction->actionidentifier   =       9;
                            $UserAction->save();
                            
                        }
                    $is_login=Auth::user();
                    $is_login->is_login     =   1;
                    $is_login->save();

                    return redirect('/home');//redirect()->route('course');
                
            }
        }
 
        return redirect('users/login')->withErrors([
            'email' => 'The email or the password is invalid. Please try again.',
        ]);
    }
    protected function postAdminLogin(LoginRequest $request) {
        //die();


            if (Auth::loginUsingId("admin", 1)) {
                
                return redirect('/admin/home');
            }
        
 
        
    }


    /* Register get post methods */
    protected function getRegister() {
         $pages=Cms::all();
        return View('users.register',compact('pages'));
    }

    protected function postRegister(RegisterRequest $request) {
/*
        if($this->captchaCheck() == false)
        {
            return redirect()->back()
                ->withErrors(['Wrong Captcha'])
                ->withInput();
        }*/
        $logintime=date('Y-m-d H:i:s');
        $this->user->name = $request->name.' '.$request->last;
        $this->user->first_name = $request->name;
        $this->user->last_name  = $request->last;
        $this->user->email = $request->email;
        $this->user->lastlogin = $request->logintime;
        $this->user->password = bcrypt($request->password);
        $confirmation_code = str_random(30);
        $this->user->confirmation_code = $confirmation_code;
        $this->user->save();
        if(Auth::loginUsingId($this->user->id)) {

                 return redirect('/company');
        }
       /*$sent = \Mail::send('email.verify', ['confirmation_code' => $confirmation_code,'user_id' => $this->user->id,'username'=>Input::get('name')], function($message) {
            $message->to(Input::get('email'), Input::get('name'))
                ->subject('Welcome to Discjockeys.com')
                ->from('management@discjockeys.com',"Management");
        });
       if($sent){
        Session::flash('alert-success', 'Check your inbox, we sent you an email to activate your account ');*/
        //return redirect('users/login');
       /* }
        else{
            echo "mail not send";
        }*/
    }
/* function to recover password */

    protected function getForgotPassword(){
      return View('users.forgot')->with('title','Members System - Forget');
     
    }
    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    protected function getLogout()
    {
        

        

        $this->auth->logout();
       
        return redirect('users/login');
    }

    protected function userVerify($confirmation_code,$user_id){
        $condition = User::where('id', '=', $user_id)
                    ->where('confirmation_code', '=',  $confirmation_code)
                    ->exists();
        if ($condition){
            DB::table('users')
                    ->where('id', '=', $user_id)
                    ->update(array('status' => '1'));
            if(Auth::loginUsingId($user_id)) {

                 return redirect('/home');
            }
            Session::flash('message', 'You are successfully verified please login');
            return redirect('users/login');
        }else{
            echo "Invalid Confirmation Code";
            die;
        }
    }
}
