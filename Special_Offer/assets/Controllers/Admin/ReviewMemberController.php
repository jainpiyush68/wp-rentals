<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Pricing;
class ReviewMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $pricing = Pricing::find(1);
        $companies=Company::where('status','=','Review')->orderBy('id','DESC')->get();
        return view('admin.reviewmember.index',compact('companies','pricing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $memberids=explode(',',$request->allmembers);
        for($i=0;$i<count($memberids);$i++){
            $Company=Company::find($memberids[$i]);
            $Company->status='Active';
            $Company->save();   
        }
       // Session::flash('alert-success', 'Members Was Approved');
        return redirect('admin/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Company=Company::find($id);
        $Company->status='Active';
        $Company->save();
        //Session::flash('alert-success', 'Company Was Approved');
        return redirect('admin/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Company=Company::find($id);
        
         $user= User::find($Company->user->id);
        $user->delete();
        $Company->delete();
        //Session::flash('alert-success', 'Company Was Deleted');
        return redirect('admin/home');
    }
}
