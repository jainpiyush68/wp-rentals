<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Cms;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages=Cms::orderBy('id', 'DESC')->get();
        return view('admin.cms.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cms= new Cms;
         $cms->title             =   $request->title;
        $cms->content           =   $request->body;
        $cms->url               =   $request->url;
        $cms->metatitle         =   $request->metatitle;
        $cms->metakeyword       =   $request->metakeyword;
        $cms->metadescription   =   $request->metadescription;
        $cms->save();
        //Session::flash('alert-success', 'Your Page Successfully Added');
        return redirect('admin/cms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cms=Cms::find($id);
        $pages=Cms::orderBy('id', 'DESC')->get();
        return view('admin.cms.edit',compact('pages','cms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cms=Cms::find($id);
         $cms->title             =   $request->title;
        $cms->content           =   $request->body;
        $cms->url               =   $request->url;
        $cms->metatitle         =   $request->metatitle;
        $cms->metakeyword       =   $request->metakeyword;
        $cms->metadescription   =   $request->metadescription;
        $cms->save();
       // Session::flash('alert-success', 'Your Page Successfully Edited');
        return redirect('admin/cms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        echo $id;
        $Cms = Cms::find($id);

       // $Cms->delete();

        return redirect('admin/cms');
    }
}
