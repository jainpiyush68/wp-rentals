<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Company;
use App\Canadazipcodes;
use App\Uszipcodes;
use App\Cms;
use App\States;
use App\Metro;
use Session;
use App\UserAction;

class MetroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages=Cms::all();
        $States=States::where('country','=',Auth::user()->company->country)->get();
        
        return view('metro.index',compact('pages','States'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $state=$request->state;
        $city=$request->city;
        $id=Auth::user()->company->id;
        for($i=0;$i<count($city);$i++) {
            $metro=New Metro;
            $metro->companyid       = $id;
            $metro->state            =$state[$i];
            $metro->city            =$city[$i];
            $metro->save();
        }
        
        if($request->metroid=='metro'){
          
            return redirect('signupsocial');
        }
        else{
            Session::flash('alert-success', 'Your metro/city was saved successfully');
            return redirect('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $state=$request->state;
        $city=$request->city;
        $metroid=$request->metroid;
        $id=Auth::user()->company->id;
         for($i=0;$i<count($state);$i++) {
            if(count($metroid)>$i){
             if(Auth::user()->company->membership==1 && $i<=1 || 
                         Auth::user()->company->membership==2 && $i<=2 ||
                         Auth::user()->company->membership==3 && $i<=3 ||
                         Auth::user()->company->membership==4 && $i<=5 ||
                         Auth::user()->company->membership==5 && $i<=7 ||
                         Auth::user()->company->membership==6 && $i<=10
                         ){
	            $metro=Metro::find($metroid[$i]);
	            $metro->companyid       = $id;
	            $metro->state            =$state[$i];
	            $metro->city            =$city[$i];
	            $metro->save();
	         }
	         else{ 
	           $metro=Metro::find($metroid[$i]);
	            
	            $metro->state            ='';
	            $metro->city            =0;
	            $metro->save();
	         }
	     }
	   else{
	   	    $metro=New Metro;
	            $metro->companyid       = $id;
	            $metro->state            =$state[$i];
	            $metro->city            =$city[$i];
	            $metro->save();
	   }
        }
        $normaluser=Auth::user();
        $adminuser=Auth::user('admin');

        if(count($normaluser)>0 && count($adminuser)==0){
            $UserAction                     =       new UserAction;
            $UserAction->user_id            =       Auth::user()->id;
            $UserAction->actionidentifier   =       2;
            $UserAction->save();
        }
        //Session::flash('alert-success', 'Metro City Search Was Added');
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
