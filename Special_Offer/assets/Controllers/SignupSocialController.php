<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Auth;

use Session;

use App\Company;

use App\Http\Requests;

use Helper;
use App\Dj_email_templates;

use App\Http\Controllers\Controller;



class SignupSocialController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        //
        $emessage=Dj_email_templates::find(1);
          $cmessage=Helper::change_message_variables($emessage->message,'Paypal transaction');
          $sent = \Mail::send('email.email', ['cmessage'=>$cmessage], function($message) use($emessage) {
                
            $message->to('jainpiyush68@gmail.com', Auth::user()->name)
               ->subject($emessage->subject)   ->from('manager@thephotoboothdirectory.com',"Photo Booth Admin"); });
      
        return view('signupSocial.index');

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

        $company=Company::find(Auth::user()->company->id);



        $company->facebook    =  $request->fbu;

        $company->youtube     =  $request->ytu;

        $company->twitter     =  $request->twu;

        $company->google      =  $request->gpu;

        $company->myspace     =  $request->msu;

        $company->pinterest   =  $request->pnu;

        $company->instagram   =  $request->igu;

       

        $company->save();

        

        if(Auth::user()->company->membership<4){
            $emessage=Dj_email_templates::find(1);
          $cmessage=Helper::change_message_variables($emessage->message,'Paypal transaction');
          $sent = \Mail::send('email.email', ['cmessage'=>$cmessage], function($message) use($emessage) {
                
            $message->to(Auth::user()->email, Auth::user()->name)
               ->subject($emessage->subject)
     ->from('manager@thephotoboothdirectory.com',"Photo Booth Admin")->sender('manager@thephotoboothdirectory.com',"Management")->replyTo('manager@thephotoboothdirectory.com',"Management");
        });
        if(!$sent){die('test');}
            Session::flash('signupconfirm', 'Thanks for fillup company Information');
            
            return redirect('home');  

        }

        else{



           return redirect('signupgallery');   

        }

        

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id
     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

}


