<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Gallery;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\UserAction;
use Image;
class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $galleries = Auth::user()->company->gallery()->orderBy('priority', 'ASC')->get();
        $user = Auth::user()->company;
        return view('gallery.index',compact('galleries','user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company = Auth::user()->company;

          
        $rand=rand();

        $gallery = new Gallery;

          if($request->order!=''){
          $gallery->priority        =   $request->order;
         $increase=$request->order +1;
        $priorities=Gallery::where('priority','=', $request->order)->where('company_id','=', $company->id)->get();
        foreach($priorities as $priority){
         $changepriority=Gallery::find($priority->gallery_id);
         $changepriority->priority  =$increase;
         $changepriority->save();

         }
        } 

        $imageName = $rand.'.'.$request->file('photo')->getClientOriginalExtension();
       
           $check=filesize($request->file('photo'));
                $check=($check/(1024*1024));
        if($check<2){
            $request->file('photo')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName);
        }
        else{

            $test=getimagesize($request->file('photo'));
            $width=$test[0]*.5;
            $height=$test[1]*.5;
            $image =$request->file('photo');
            $path = public_path('assets/gallery/'.$company->id.'/'.$imageName);
            Image::make($image->getRealPath())->resize($width, $height)->save($path);

        }
 

        $gallery->image        =   $imageName;

        $gallery->caption      =   $request->caption;

        $gallery->company_id   =   $company->id;

        $gallery->save();

        // $rand2=rand();

        // $gallery = new Gallery;

        // $imageName2 = $rand2.'.'.$request->file('photo2')->getClientOriginalExtension();
       
        // $request->file('photo2')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName2);

        // $gallery->image        =   $imageName2;

        // $gallery->company_id     =   $company->id;

        // $gallery->save();
        $normaluser=Auth::user();
        $adminuser=Auth::user('admin');

        if(count($normaluser)>0 && count($adminuser)==0){
            $UserAction                     =       new UserAction;
            $UserAction->user_id            =       Auth::user()->id;
            $UserAction->actionidentifier   =       4;
            $UserAction->save();
        }

        Session::flash('alert-success', 'Photo was succesfully uploaded');

        return redirect('gallery');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $gallery=Gallery::find($id);
        return view('gallery.edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       $company = Auth::user()->company;
        
        
          if($request->order!=''){
          
         $increase=$request->order +1;
        $priorities=Gallery::where('priority','=', $request->order)->where('company_id','=', $company->id)->get();
        foreach($priorities as $priority){
         $changepriority=Gallery::find($priority->gallery_id);
         $changepriority->priority  =$increase;
         $changepriority->save();

         }
        
        }
        
        $gallery=Gallery::find($id);
        if($request->order!=''){
        $gallery->priority        =   $request->order;
        } 
        $gallery->caption      =   $request->caption;
        $gallery->save();
        Session::flash('alert-success', 'Photo caption was updated succesfully');
        return redirect('gallery');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $gallery=Gallery::find($id);
        $gallery->delete();

        return redirect('gallery');
    }

    public function ordering(Request $request){
        $count = 1;
        $id_array=explode(',',$request->ids);
            foreach ($id_array as $id){
            $gallery=Gallery::find($id);
            $gallery->priority  =$count;
            $gallery->save();
            $count ++;  
        }

    }
}
