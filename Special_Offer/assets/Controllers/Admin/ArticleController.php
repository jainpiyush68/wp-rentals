<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Articles;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages=Articles::orderBy('priority','ASC')->orderBy('updated_at','DESC')->get();
        return view('admin.article.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          if($request->priority!=''){
         $increase=$request->priority +1;
        $priorities=Articles::where('priority','=', $request->priority)->get();
        foreach($priorities as $priority){
         $changepriority=Articles::find($priority->id);
         $changepriority->priority  =$increase;
         $changepriority->save();

         }
        }
        $article= new Articles;
        $article->title     =   $request->title;
        $article->body      =   $request->body;
        $article->priority  =   $request->priority;
        $article->save();
        //Session::flash('alert-success', 'Your Page Successfully Added');
        return redirect('admin/article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article = Articles::find($id);

        $article->delete();

        return redirect('admin/article');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $articles=Articles::find($id);
        $pages=Articles::orderBy('priority','ASC')->orderBy('updated_at','DESC')->get();
        return view('admin.article.edit',compact('pages','articles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          if($request->priority!=''){
         $increase=$request->priority +1;
        $priorities=Articles::where('priority','=', $request->priority)->get();
        foreach($priorities as $priority){
         $changepriority=Articles::find($priority->id);
         $changepriority->priority  =$increase;
         $changepriority->save();

         }
        }
        $article= Articles::find($id);
        $article->title     =   $request->title;
        $article->body      =   $request->body;
        $article->priority  =   $request->priority;
        $article->save();
         return redirect('admin/article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Articles::find($id);

       // $article->delete();

        return redirect('admin/article');
    }
}
