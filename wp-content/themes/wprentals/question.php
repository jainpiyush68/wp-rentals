<?php
// Template Name:User Questions and answer's

// Wp Estate Pack
if ( !is_user_logged_in() ) {   
  //wp_redirect( home_url('url') );
} 

$current_user = wp_get_current_user();
$userID                         =   $current_user->ID;
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_phone_number              =   get_user_meta($current_user->ID,'phone',true);
$status_values                  =   esc_html( get_option('wp_estate_status_list') );
$status_values_array            =   explode(",",$status_values);
$feature_list_array             =   array();
$feature_list                   =   esc_html( get_option('wp_estate_feature_list') );
$feature_list_array             =   explode( ',',$feature_list);
$allowed_html                   =   array();
global $show_err;

///////////////////////////////////////////////////////////////////////////////////////////
/////// Submit Code
///////////////////////////////////////////////////////////////////////////////////////////
if( 'POST' == $_SERVER['REQUEST_METHOD'] ) {

    
    // if ( !sh_verify_onetime_nonce( $_POST['estatenonce'], 'thisestate') ){
          
    //    exit('1');
    // }else{
     
    // }
    
            

} 



get_header();
$options=wpestate_page_details($post->ID); 
global $more;
$more = 0;
$wide_page = get_post_meta($post->ID, 'wide_page', true);
$wide_class='';
?>

              
<div id="post" <?php post_class('row  '.$wide_class);?>>
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print $options['content_class'];?> ">
        <?php get_template_part('templates/ajax_container'); ?>
        
        <div class="single-content">
            <?php 
            global $more;
            $more=0;
           // while ( have_posts() ) : the_post();
            if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title single-title" ><?php the_title(); ?></h1>
                <div class="meta-element-head"> 
                    <?php print the_date('', '', '', FALSE).' '.esc_html__( 'by', 'wpestate').' '.get_the_author();  ?>
                </div>
                
            <?php 
            if ( !is_user_logged_in() ) {   ?>
            <div class="col-md-6">

            
            <!-- <form id="custom_login_for_tenant_question" action=""  method="post"> -->
                <div class="loginalert" id="register_message_area"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>User Name</label>
                        <input type="text" name="user_login_register" id="user_login_register" class="form-control" placeholder="Username" size="20">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>User Name</label>
                        <input type="text" name="user_email_register" id="user_email_register" class="form-control" placeholder="Email" size="20">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password" size="20">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="user_password_retype" id="user_password_retype" class="form-control " placeholder="Retype Password" size="20">
                    </div>
                </div>
                <input type="hidden" name="acc_type" id="acctype0" value="1" checked="" required="">
                
                <input type="checkbox" name="terms" id="user_terms_register_sh">
                
                <label id="user_terms_register_sh_label" for="user_terms_register_sh">I agree with <a href="https://foundmyroom.com" target="_blank" id="user_terms_register_topbar_link">terms &amp; conditions</a> </label>
                <button id="wp-submit-register-question" style="margin-top:10px;" class="wpb_button  wpb_btn-info  wpb_btn-small wpestate_vc_button  vc_button
                pull-right">Create an account</button>
            <!-- </form> -->
            </div>
            <div class="col-md-6">
                <!-- <div class="loginalert" id="login_message_area"></div> -->
                <div class="loginrow">
                    <label>Username</label>
                    <input type="text" class="form-control" name="log" id="login_user" placeholder="Username" size="20">
                </div> 
                <div class="loginrow">
                    <label>Password</label>
                    <input type="password" class="form-control" name="pwd" placeholder="Password" id="login_pwd" size="20">
                </div>
                <!-- <input type="hidden" name="loginpop" id="loginpop_wd" value="0"> -->
                      

                <input type="hidden" id="security-login" name="security-login" value="<?php echo estate_create_onetime_nonce( 'login_ajax_nonce' )?>">
                <button id="wp-login-but-question" class="wpb_button  wpb_btn-info  wpb_regularsize   wpestate_vc_button  vc_button" data-mixval="0">Login</button>
            </div>
            <?php

  //wp_redirect( home_url('url') );
} else{
            questions();
        }
            } 
        
            ?>          
        </div>    
     
        <!-- #comments start-->
        <?php // comments_template('', true);?>     
        <!-- end comments -->   
        
        <?php //endwhile; // end of the loop. ?>
    </div>
       
<?php  include(locate_template('sidebar.php')); ?>
</div>   

<?php get_footer(); ?>