<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Admin;
use Input;
use Validator;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminUserContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adminusers     =   Admin::all();
        return view('admin.adminuser.index',compact('adminusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $rules = [
            'name' => 'required|unique:admin',
            'email' => 'required|email|unique:admin',
            'password' => 'required|min:6'
        ];

        $input = Input::only(
            'name',
            'email',
            'password'
           
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $adminuser              =       new Admin;
        $adminuser->name        =       $request->name;
        $adminuser->email       =       $request->email;
        $adminuser->password    =       bcrypt($request->password);
        $adminuser->save();
        return redirect('/admin/adminuser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       $adminuser              =       Admin::find($id);
        $adminuser->delete();
        return redirect('/admin/adminuser');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $adminuser      =   Admin::find($id);
        $adminusers     =   Admin::all();
        return view('admin.adminuser.edit',compact('adminuser','adminusers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //
         $adminuser              =       Admin::find($id);
          $rules = [
            'name' => 'required|unique:admin,name,'.$adminuser->id,
            'email' => 'required|email|unique:admin,email,'.$adminuser->id,
            
        ];

        $input = Input::only(
            'name',
            'email'           
           
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
       
        $adminuser->name        =       $request->name;
        $adminuser->email       =       $request->email;
       
        $adminuser->save();
        return redirect('/admin/adminuser');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $adminuser              =       Admin::find($id);
        $adminuser->delete();
        return redirect('/admin/adminuser');
    }
}
