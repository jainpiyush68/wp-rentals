<?php
// Template Name:Stripe Charge Page
// Wp Estate Pack
require_once get_template_directory().'/libs/stripe/lib/Stripe.php';
$allowed_html=array();
$stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
$stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
$stripe = array(
    "secret_key"      => $stripe_secret_key,
    "publishable_key" => $stripe_publishable_key
);

Stripe::setApiKey($stripe['secret_key']);     
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////webhook part
//////////////////////////////////////////////////////////////////////////////////////////////////



// Retrieve the request's body and parse it as JSON

$input = wp_remote_get("php://input");
//$event_json = wp_remote_retrieve_body( $request );

/*

if($event_json!=''){
    $event_json = json_decode($input);
   
    $array=get_object_vars($event_json->data);
    foreach($array as $key=>$value){
        $customer_stripe_id= $value->customer;
    }

  
    if($event_json->type=='charge.failed'){
        $args   =   array(  'meta_key'      => 'stripe', 
                            'meta_value'    => $customer_stripe_id
                        );
        
        $customers  =   get_users( $args ); 
        foreach ( $customers as $user ) {
            update_user_meta( $user->ID, 'stripe', '' );
            downgrade_to_free($user->ID);
        }        
    }  
    
    
    // recurring charge
    if($event_json->type=='charge.succeeded'){
            $args=array('meta_key'      => 'stripe', 
                        'meta_value'    => $customer_stripe_id
            );
            
            $update_user_id =   0;
            $customers=get_users( $args ); 
            foreach ( $customers as $user ) {
                $update_user_id = $user->ID;
            } 
            $pack_id = intval (get_user_meta($update_user_id, 'package_id',true));
           
            if($update_user_id!=0 && $pack_id!=0){
                if( wpestate_check_downgrade_situation($update_user_id,$pack_id) ){
                    wpestate_downgrade_to_pack( $update_user_id, $pack_id );
                    wpestate_upgrade_user_membership($update_user_id,$pack_id,2,'');
                }else{
                    wpestate_upgrade_user_membership($update_user_id,$pack_id,2,'');
                }     
            
            }else{
               // echo 'no user';           
            }    
    }
    
    http_response_code(200); 
    exit();
}
*/
//////////////////////end webhook - start processing

if( is_email($_POST['stripeEmail']) ){
    $stripeEmail= wp_kses($_POST['stripeEmail'],$allowed_html)  ;
}else{
    exit('none mail');
}

if( isset($_POST['userID']) && !is_numeric( $_POST['userID'] ) ){
    exit();
}

if( isset($_POST['invoice_id']) && !is_numeric( $_POST['invoice_id'] ) ){
    exit();
}

if( isset($_POST['booking_id']) && !is_numeric( $_POST['booking_id'] ) ){
    exit();
}

if( isset($_POST['depozit']) && !is_numeric( $_POST['depozit'] ) ){
    exit();
}

if( isset($_POST['pack_id']) && !is_numeric( $_POST['pack_id'] ) ){
    exit();
}

if( isset($_POST['pay_ammout']) && !is_numeric( $_POST['pay_ammout'] ) ){
    exit();
}

if( isset($_POST['stripe_recuring']) && !is_numeric( $_POST['stripe_recuring'] ) ){
    exit();
}

if( isset($_POST['submission_pay']) && !is_numeric( $_POST['submission_pay'] ) ){
    exit();
}

if( isset($_POST['propid']) && !is_numeric( $_POST['propid'] ) ){
    exit();
}

if( isset($_POST['featured_pay']) && !is_numeric( $_POST['featured_pay'] ) ){
    exit();
}

if( isset($_POST['is_upgrade']) && !is_numeric( $_POST['is_upgrade'] ) ){
    exit();
}



$current_user = wp_get_current_user();
$userID         =   $current_user->ID;
$user_email     =   $current_user->user_email;
$username       =   $current_user->user_login;
$submission_curency_status = esc_html( get_option('wp_estate_submission_curency','') );

////////////////////////////////////////////////////////////////////////////////
///////////////// payment for booking 
////////////////////////////////////////////////////////////////////////////////
if ( isset($_POST['booking_id']) ){
    try {    
        $token  = wp_kses($_POST['stripeToken'],$allowed_html);
        $customer = Stripe_Customer::create(array(
            'email' => $stripeEmail,
            'card'  => $token
        ));

        $userId     =   intval($_POST['userID']);
        $invoice_id =   intval($_POST['invoice_id']);
        $booking_id =   intval($_POST['booking_id']);
        $depozit    =   intval($_POST['depozit']);
        $charge = Stripe_Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $depozit,
            'currency' => $submission_curency_status
        ));


        // confirm booking
        update_post_meta($booking_id, 'booking_status', 'confirmed');

        $curent_listng_id   =   get_post_meta($booking_id,'booking_id',true);
        $reservation_array  =   wpestate_get_booking_dates($curent_listng_id);

        //$subject= "test";
       update_post_meta($curent_listng_id, 'booking_dates', $reservation_array); 

        // set invoice to paid
        update_post_meta($invoice_id, 'invoice_status', 'confirmed');
        update_post_meta($invoice_id, 'depozit_paid', ($depozit/100) );

        /////////////////////////////////////////////////////////////////////////////
        // send confirmation emails
        /////////////////////////////////////////////////////////////////////////////   
        //wpestate_send_booking_email("bookingconfirmeduser",$user_email);
        $receiver_id    =   wpsestate_get_author($invoice_id);
        $receiver_email =   get_the_author_meta('user_email', $receiver_id); 
        $receiver_name  =   get_the_author_meta('user_login', $receiver_id); 
        $reciever_phone = get_user_meta($receiver_id,'userphone',true);

        $user_phone = get_user_meta($userID,'userphone',true);

        $booking_from_date = get_post_meta($booking_id,'booking_from_date',true);
        $booking_to_date = get_post_meta($booking_id,'booking_to_date',true);
        $is_student=get_user_meta($userID,'verified_student',true);
        $property_id = $curent_listng_id;
        $property_stamps_selected     =   get_the_terms($property_id, 'property_stamps');
        $booking_array      =   wpestate_booking_price($booking_guests,$invoice_id,$property_id, $booking_from_date, $booking_to_date);
             if(isset($property_stamps_selected[0])){
                $property_stamps_selected           =   $property_stamps_selected[0]->slug;
             }
            $first_month_price   =   $booking_array['default_price'];
            $custom_price_array  =   wpml_custom_price_adjust($property_id);

            $lastdate= date('t',strtotime($booking_from_date));
            $timestamp_java=strtotime($booking_from_date);
            if( array_key_exists  ($timestamp_java,$custom_price_array) ){
                $first_month_price_first= $custom_price_array[$timestamp_java];
             }
            if($booking_array['has_custom'] == 1){
                $first_month_price=$first_month_price_first;
            } 
             $booking_charges=0;
            if($is_student==1){
                    $booking_charges=10;
            }
            else{
                    
                if($booking_array['numberDays']<30){
                    $booking_days_for_formula=30;
                }
                elseif ($booking_array['numberDays']>360) {
                    $booking_days_for_formula=360;
                }
                else{
                    $booking_days_for_formula=$booking_array['numberDays'];
                }
                 //$first_month_price=($booking_array['total_price']/$booking_array['numberDays'])*30;
                if($property_stamps_selected=='simply-green' && $verifed_property==1){
                    $booking_charges    =   ($first_month_price) *((0.00005828*$booking_days_for_formula**2+19.9475)/100);
                }
                elseif ($property_stamps_selected=='just-blue' && $verifed_property==1) {
                    $booking_charges    =  ($first_month_price) *(( 0.00005828*$booking_days_for_formula**2+12.4475)/100);
                }
                else{
                    //$booking_charges    =  ($first_month_price) *((0.00005828*$booking_days_for_formula**2+4.4758)/100);
                     $booking_charges    =  ($first_month_price) *((10)/100);
                }
        }
        //echo "string";
        $total_booking_chages   =  $first_month_price+$booking_charges;
        $first_month_price_show     =   wpestate_show_price_booking($first_month_price,$currency,$where_currency,1);
        $custom_total_price     =   wpestate_show_price_booking($total_booking_chages,$currency,$where_currency,1);
        $custom_booking_charges =   wpestate_show_price_booking($booking_charges,$currency,$where_currency,1);

      
       
        $headers        =   array('"MIME-Version: 1.0\r\n"' , "Content-Type: text/html; charset=ISO-8859-1\r\n");

        $imagesUrl      =   get_template_directory_uri().'/images/';
        $emailMessages  =   file_get_contents(WP_CONTENT_DIR . '/themes/wprentals/tenantPayConfirmation.html');  
               
        $emailMessages  =   str_replace('%name%', $username, $emailMessages);
        $emailMessages  =   str_replace('%invoice_number%', $invoice_id, $emailMessages);
        $emailMessages  =   str_replace('images/', $imagesUrl, $emailMessages);

        wp_mail($user_email,'Payment Confirmed',$emailMessages,$headers);

        $messages   = "Hi there, the room is booked! <br><br>";

        $messages  .=  "Now, just relax and look forward to your new home. <br><br>";

        $messages  .=  "Those are the details for you again:<br><br><br>";

        $messages  .= "Name of the Host : $receiver_name<br><br>";
        //$messages  .= "Address <br> <br>";
        $messages  .= "Phone Number : $reciever_phone <br><br> ";
        $messages  .= "Move In Date : $booking_from_date   Move out Date : $booking_to_date <br> <br><br>";
      
        $messages  .= "The Monthly Rent Cost : $first_month_price_show <br><br> ";
        $messages  .= "The Service Fee : $custom_booking_charges<br> <br>";
        $messages  .=  "Of course, you can see all the details and more in the ('invoice'link) section on your profile. <br><br><br>";
        $messages  .=  "We wish you the best for your trip and don´t forget. Traveling around the world means, understanding it<br><br><br>";
        $messages  .=  "If you have any questions please do not hesitate to write us at home@foundmyroom.com.<br><br><br>";
        $messages  .= "Best Your <br> ";
        $messages  .= "FoundMyRoom Team <br> <br>";
        $subject    = "Booking Confirmed on ".site_url();
        wp_mail($user_email,$subject,$messages,$headers);
        
       
          $booking_charges=0;
            if($booking_array['numberDays']<30){
                $booking_days_for_formula=30;
            }

            elseif ($booking_array['numberDays']>360) {
                $booking_days_for_formula=360;
            }
            else{
                $booking_days_for_formula=$booking_array['numberDays'];
            }
            if($booking_array['numberDays']<=120){
                $simply_green_first     = 0.00223;
                $simply_green_second    = 1.403;
                $just_blue_first        = 0.001667;
                $just_blue_second       = 1.5003;
            }
            else{
                $simply_green_first     = 0.00056424;
                $simply_green_second    = 26.88;
                $just_blue_first        = 0.000651;
                $just_blue_second       = 15.6256;
            }
            $verifed_property   =   get_post_meta($property_id,'verified',true);
            // $first_month_price=($booking_array['total_price']/$booking_array['numberDays'])*30;
            if($property_stamps_selected=='simply-green' && $verifed_property==1){
                $booking_charges    =   ($first_month_price) *(($simply_green_first*$booking_days_for_formula**2+$simply_green_second)/100);
            }
            elseif ($property_stamps_selected=='just-blue' && $verifed_property==1) {
                $booking_charges    =  ($first_month_price) *(( $just_blue_first*$booking_days_for_formula**2+$just_blue_second )/100);
            }
            else{
                //$booking_charges    =  ($first_month_price) *((0.00005828*$booking_days_for_formula**2+2.4475)/100);
                $booking_charges    =  ($first_month_price) *((10)/100);
            }
            $booking_custom_fee     =   'Commission';
            $total_booking_chages   =   $first_month_price-$booking_charges;
            $custom_total_price     =   wpestate_show_price_booking($total_booking_chages,$currency,$where_currency,1);
            $first_month_price_show =    wpestate_show_price_booking($first_month_price,$currency,$where_currency,1);
            $custom_booking_charges =   wpestate_show_price_booking($booking_charges,$currency,$where_currency,1);

         $messages   = "Hi there, the room is booked! <br><br>";

        $messages  .=  "Now, just relax and look forward to your new home. <br><br>";

        $messages  .=  "Those are the details for you again:<br><br>";

        $messages  .= "Name of the Tenant : $username <br> <br>";
        //$messages  .= "Address <br> <br>";
        $messages  .= "Phone Number : $user_phone <br> <br>";
        $messages  .= "Move In Date : $booking_from_date   Move out Date : $booking_to_date <br> <br><br>";
      
        $messages  .= "The Monthly Rent Cost : $first_month_price_show <br><br> ";
        $messages  .= "The Service Fee : $custom_booking_charges<br><br><br> ";
        $messages  .=  "Of course, you can see all the details and more in the ('invoice'link) section on your profile. <br><br><br>";
        $messages  .=  "After taking the service fee, the first rent will be transferred on the $booking_from_date from '$username'. <br><br><br>";
        $messages  .=  "If you have any questions please do not hesitate to write us at home@foundmyroom.com.<br><br><br>";
        $messages  .= "Best Your <br> ";
        $messages  .= "FoundMyRoom Team <br> <br>";
        wp_mail($receiver_email,$subject,$messages,$headers);
        //wpestate_send_booking_email("bookingconfirmed",$receiver_email);


        // add messages to inbox
        $subject=esc_html__( 'Booking Confirmation','wpestate');
        $description=esc_html__( 'A booking was confirmed','wpestate');
        wpestate_add_to_inbox($userID,$userID,$receiver_id,$subject,$description,1);

        ////redirect catre bookng list
        $redirect=wpestate_my_reservations_link();
         wp_redirect($redirect);
        exit();
    }
    catch (Exception $e) {
        $error =    '<div class="alert alert-danger">
                        <strong>Error!</strong> '.$e->getMessage().'
                    </div>';
        print $error;
    }

    
}else if ( isset ($_POST['submission_pay'])  && $_POST['submission_pay']==1  ){
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////// payment for submission
    ////////////////////////////////////////////////////////////////////////////////    
    try {
        $token  = wp_kses($_POST['stripeToken'],$allowed_html);
        $customer = Stripe_Customer::create(array(
            'email' => $stripeEmail,
            'card'  => $token
        ));

        $userId         =   intval($_POST['userID']);
        $listing_id     =   intval($_POST['propid']);
        $pay_ammout     =   intval($_POST['pay_ammout']);
        $is_featured    =   0;
        $is_upgrade     =   0;
        
        if ( isset($_POST['featured_pay']) && $_POST['featured_pay']==1 ){
            $is_featured    =   intval($_POST['featured_pay']);
        }

        if ( isset($_POST['is_upgrade']) && $_POST['is_upgrade']==1 ){
            $is_upgrade    =   intval($_POST['is_upgrade']);
        }
        
        $charge = Stripe_Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $pay_ammout,
            'currency' => $submission_curency_status
        ));
        
      
        $time = time(); 
        $date = date('Y-m-d H:i:s',$time);
    
        if($is_upgrade==1){
            update_post_meta($listing_id, 'prop_featured', 1);
            $invoice_id = wpestate_insert_invoice('Upgrade to Featured','One Time',$listing_id,$date,$current_user->ID,0,1,'' );
            update_post_meta($invoice_id, 'invoice_status', 'confirmed');
            wpestate_email_to_admin(1);
        }else{
            update_post_meta($listing_id, 'pay_status', 'paid');
            $admin_submission_status = esc_html ( get_option('wp_estate_admin_submission','') );
            $paid_submission_status  = esc_html ( get_option('wp_estate_paid_submission','') );
 
            if($admin_submission_status=='no'  && $paid_submission_status=='per listing' ){
                $post = array(
                    'ID'            => $listing_id,
                    'post_status'   => 'publish'
                    );
                $post_id =  wp_update_post($post ); 
            }
            // end make post publish

        if($is_featured==1){
            update_post_meta($listing_id, 'prop_featured', 1);
            $invoice_id = wpestate_insert_invoice('Publish Listing with Featured','One Time',$listing_id,$date,$current_user->ID,1,0,'' );
            update_post_meta($invoice_id, 'invoice_status', 'confirmed');
        }else{
            $invoice_id = wpestate_insert_invoice('Listing','One Time',$listing_id,$date,$current_user->ID,0,0,'' );
            update_post_meta($invoice_id, 'invoice_status', 'confirmed');
        }
        wpestate_email_to_admin(0);
        }
        
        $redirect = wpestate_get_dashboard_link();
        wp_redirect($redirect);exit();
        
    }
    catch (Exception $e) {
    $error =    '<div class="alert alert-danger">
                    <strong>Error!</strong> '.$e->getMessage().'
                </div>';
    print $error;
    }
    
}else if ( isset ($_POST['stripe_recuring'] ) && $_POST['stripe_recuring'] ==1 ) { 
////////////////////////////////////////////////////////////////////////////////
////////////////// payment for pack recuring
////////////////////////////////////////////////////////////////////////////////    
    try {
        $dash_profile_link = wpestate_get_dashboard_profile_link();
        $token          =   wp_kses($_POST['stripeToken'],$allowed_html);
        $pack_id        =   intval($_POST['pack_id']);
        $stripe_plan    =   esc_html(get_post_meta($pack_id, 'pack_stripe_id', true));

        $customer = Stripe_Customer::create(array(
            "card" =>  $token,
            "plan" =>  $stripe_plan,
            "email" => $stripeEmail
          ));
       
        
        $stripe_customer_id=$customer->id;
        $subscription_id = $customer->subscriptions['data'][0]['id'];
 
        if( wpestate_check_downgrade_situation($current_user->ID,$pack_id) ){
            wpestate_downgrade_to_pack( $current_user->ID, $pack_id );
            wpestate_upgrade_user_membership($current_user->ID,$pack_id,2,'');
        }else{
            wpestate_upgrade_user_membership($current_user->ID,$pack_id,2,'');
        }      
        update_user_meta( $current_user->ID, 'stripe', $stripe_customer_id );
        update_user_meta( $current_user->ID, 'stripe_subscription_id', $subscription_id );
        wp_redirect( $dash_profile_link ); exit();
 
    }
    catch (Exception $e) {
        $error = '  <div class="alert alert-danger">
                        <strong>Error!</strong> '.$e->getMessage().'
                    </div>';
        print $error;
    }
 
    
}else{

////////////////////////////////////////////////////////////////////////////////
////////////////// payment for pack
////////////////////////////////////////////////////////////////////////////////  
    try {
        $dash_profile_link = wpestate_get_dashboard_profile_link();

        $token  =  wp_kses($_POST['stripeToken'],$allowed_html);
        $customer = Stripe_Customer::create(array(
            'email' => $stripeEmail,
            'card'  => $token
        ));

        $userId     =   intval($_POST['userID']);
        $pay_ammout =   intval($_POST['pay_ammout']);
        $pack_id    =   intval($_POST['pack_id']);
        $charge     =   Stripe_Charge::create(array(
                        'customer' => $customer->id,
                        'amount'   => $pay_ammout,
                        'currency' => $submission_curency_status
        ));

        if( wpestate_check_downgrade_situation($current_user->ID,$pack_id) ){
            wpestate_downgrade_to_pack( $current_user->ID, $pack_id );
            wpestate_upgrade_user_membership($current_user->ID,$pack_id,1,'');
        }else{
            wpestate_upgrade_user_membership($current_user->ID,$pack_id,1,'');
        }      
        wp_redirect( $dash_profile_link );   exit();   
    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                    <strong>Error!</strong> '.$e->getMessage().'
                </div>';
        print $error;
    }
    
}

?>