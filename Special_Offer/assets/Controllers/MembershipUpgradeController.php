<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;

use App\Company;
use App\Membership;
use App\Pricing;
use App\Banner;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembershipUpgradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        if(Auth::user()->company->membership=='6')
            return redirect('message');
        $memberships=Membership::all();
        $user = Auth::user()->company;
        $pricing = Pricing::find(1);
         $date=date_create(Auth::user()->company->expirationdate);
            
            $expiredate=date_format($date,"Y-m-d");
            $date1=date_create($expiredate);
            $date2=date_create("today");
            $diff=date_diff($date2,$date1);
$dayLeft="0 days";
 if(Auth::user()->company->expirationdate!='0000-00-00 00:00:00' || Auth::user()->company->expirationdate!='')
             $dayLeft=$diff->format("%a days");
            $dayLeft2=$diff->format("%a");
            $nextexpiration=date_create("today");
            date_add($nextexpiration,date_interval_create_from_date_string("1 year"));
            $nextexpiration=date_format($nextexpiration,"m-d-Y");
            $lastpaymentpaid= (Auth::user()->company->lastpayment!= '' ? Auth::user()->company->lastpayment : 0);
           
            $credit= round(($lastpaymentpaid/365)*$dayLeft2,2);
            $expiredate=date_format($date,"m-d-Y");
        return view('membership.index',compact('user','pricing','memberships','credit','dayLeft','expiredate','nextexpiration'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pricing = Pricing::find(1);
        $user = Auth::user();
        $pricing=Pricing::find(1);
        $dayLeft=0;
        $credit=0;
       Session::put('membership', $request->membership);
        return redirect('membershippay');
        if(count(Auth::user()->company)>0){
           
            $date=date_create(Auth::user()->company->expirationdate);
            //date_add($date,date_interval_create_from_date_string("1 year"));
            $expiredate=date_format($date,"Y-m-d");
            $date1=date_create($expiredate);
            $date2=date_create("today");
            $diff=date_diff($date2,$date1);
            $dayLeft=$diff->format("%a days");
           
            $credit= (Auth::user()->company->lastpayment/365)*$dayLeft;
        }
        $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
        if($request->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['up_free']-$credit;
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif($request->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['up_basic']-$credit;
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif($request->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['up_bronze']-$credit;
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif($request->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['up_silver']-$credit;
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif($request->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['up_gold']-$credit;
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif($request->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['up_platinum']-$credit;
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;

         $ds=round($discountprice,2);
       

          
        $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '108',
           
            
            'PAYMENTREQUEST_0_AMT' =>$ds,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => $ds,
          
            'L_PAYMENTREQUEST_0_NAME0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_DESC0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_QTY0' => 1,
            'L_PAYMENTREQUEST_0_AMT0' =>$ds,
            'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Physical',
          
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => 'Photo Booth Directory Membership',
          
            'CANCELURL' => url('memberupgrade'),
            'RETURNURL' => url("home?membership=$request->membership"),
            'NOTIFYURL' => url('signupreview'),

        )));
         
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $nvp = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $nvp[$name] = urldecode($matches['value'][$offset]);
            }
        }
         
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $query = array(
                'cmd'    => '_express-checkout',
                'token'  => $nvp['TOKEN']
            );
         
            $redirectURL = sprintf('https://www.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
         
            header('Location: ' . $redirectURL);
        } else {
            //Opz, alguma coisa deu errada.
            //Verifique os logs de erro para depuração.
        } 
          die();
        //return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
            $companies=Company::where('status','=','Active')->get();
            foreach ($companies as $key => $company) {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
                  
                  
                    'METHOD' => 'GetRecurringPaymentsProfileDetails',
                    'VERSION' => '108',
                    
                  
                    
                    'PROFILEID' => $company->profileid,
                  
                    
                )));
                  
                $response =    curl_exec($curl);
                  
                curl_close($curl);
                  
                $profilecheck = array();
                  
                if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                    foreach ($matches['name'] as $offset => $name) {
                        $profilecheck[$name] = urldecode($matches['value'][$offset]);
                    }
                }
                $date=date_create($company->expirationdate);
                //date_add($date,date_interval_create_from_date_string("1 year"));
                $expiredate=date_format($date,"Y-m-d");
                $date1=date_create($expiredate);
                $date2=date_create("today");
                $diff=date_diff($date2,$date1);
                $dayLeft=$diff->format("%R%a days");
                if($dayLeft<=0)
                {
                    if($profilecheck['STATUS']=='Active'){
                        
                        $date2=date_create("today");
                        date_add($date2,date_interval_create_from_date_string("1 year"));
                        $company=Company::find($company->id);
                       
                        $company->expirationdate =  $date2;
                        $company->lastpayment    =  $profilecheck['AMT'];
                        
                        $company->status         =  'Active';
                        $company->save();
                        
                      
                    }
                    elseif($profilecheck['STATUS']=='Suspended' || $profilecheck['STATUS']=='Cancelled'){
                        
                        
                        $company=Company::find($company->id);
                        $company->membership     =  1;
                        $company->expirationdate =  '';
                        $company->lastpayment    =  '';
                        $company->payerid        =  '';
                        $company->profileid      =  '';
                        $company->status         =  'Active';
                        $company->save();

                        $Banner=Banner::find($company->user->banner->id);
                        $Banner->status     =   'Inactive';
                        $Banner->save();
                      
                    }
                    echo "success";
                }
                echo "<pre>";
                print_r($profilecheck);
                die;
            }
       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}