<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Company;
use App\Pricing;
use App\User;
use Auth;
use Response;
use App\Canadazipcodes;
use App\Uszipcodes;
use Helper;
use App\Dj_email_templates;
use Input;
use Omnipay\Omnipay;
use Omnipay\CreditCard;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaypalpayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $pricing = Pricing::find(1);
         $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
         if(Auth::user()->company->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['sn_free'];
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif(Auth::user()->company->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif(Auth::user()->company->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif(Auth::user()->company->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif(Auth::user()->company->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif(Auth::user()->company->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;

          return view('paypal.index',compact('discountprice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           $pricing = Pricing::find(1);
         $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
         if(Auth::user()->company->membership==1):
           $discountprice=$pricing['pr_free']-$pricing['sn_free'];
           $saveprice=$pricing['pr_free']-$discountprice;
        elseif(Auth::user()->company->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic']-$discountprice;
        elseif(Auth::user()->company->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze']-$discountprice;
        elseif(Auth::user()->company->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver']-$discountprice;
        elseif(Auth::user()->company->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold']-$discountprice;
        elseif(Auth::user()->company->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum']-$discountprice;
        endif;

          $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
             'USER' => 'PayPal_api1.DiscJockeys.com',
            'PWD' => 'HUWQKD926WUVK5A8',
            'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA-k-P-qlKseaM-8-4en0OrdbeCP',
          
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '93',
           
            
             'PAYMENTREQUEST_0_AMT' =>$discountprice,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => $discountprice,
          
               'L_PAYMENTREQUEST_0_NAME0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_DESC0' => 'Photo Booth Directory Membership',
            'L_PAYMENTREQUEST_0_QTY0' => 1,
            'L_PAYMENTREQUEST_0_AMT0' => $discountprice,
            'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Physical',
           
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => 'Photo Booth Directory Membership',
          
            'CANCELURL' => url('pay'),
            'RETURNURL' => url('signupreview'),
            'NOTIFYURL' => url('signupreview'),        )));
         
        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $nvp = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $nvp[$name] = urldecode($matches['value'][$offset]);
            }
        }
         //print_r($nvp);
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $query = array(
                'cmd'    => '_express-checkout',
                'token'  => $nvp['TOKEN']
            );
         
            $redirectURL = sprintf('https://www.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
         
            header('Location: ' . $redirectURL);
        } else {
            //Opz, alguma coisa deu errada.
            //Verifique os logs de erro para depuração.
        } 


            
           
    die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function creditcard(Request $request)
    {
        //
  
          $pricing = Pricing::find(1);
         $date=date_create("today");
        date_add($date,date_interval_create_from_date_string("1 year"));
         if(Auth::user()->company->membership==1):
           $discountprice=$pricing['pr_free'];
           $saveprice=$pricing['pr_free'];
        elseif(Auth::user()->company->membership==2):
           $discountprice=$pricing['pr_basic']-$pricing['sn_basic'];
           $saveprice=$pricing['pr_basic'];
        elseif(Auth::user()->company->membership==3):
            $discountprice=$pricing['pr_bronze']-$pricing['sn_bronze'];
            $saveprice=$pricing['pr_bronze'];
        elseif(Auth::user()->company->membership==4):
            $discountprice=$pricing['pr_silver']-$pricing['sn_silver'];
            $saveprice=$pricing['pr_silver'];
        elseif(Auth::user()->company->membership==5):
            $discountprice=$pricing['pr_gold']-$pricing['sn_gold'];
            $saveprice=$pricing['pr_gold'];
        elseif(Auth::user()->company->membership==6):
            $discountprice=$pricing['pr_platinum']-$pricing['sn_platinum'];
            $saveprice=$pricing['pr_platinum'];
        endif;
        
        date_default_timezone_set("UTC");
       
        $date=date('Y-m-d H:i:s', strtotime("+1 Year"));
       /* $curl = curl_init();
          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, env('PAYPAL_URL'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'USER' => env('PAYPAL_USERNAME'),
            'PWD' => env('PAYPAL_PASSWORD'),
            'SIGNATURE' => env('PAYPAL_SIGNATURE'),
          
          
            'METHOD' => 'CreateRecurringPaymentsProfile',
            'VERSION' => '108',
            
       
          
            'PROFILESTARTDATE' => $date,
            'DESC' => 'Photo Booth Directory Membership',
            'BILLINGPERIOD' => 'Year',
            'BILLINGFREQUENCY' => '1',
            'AMT' => $saveprice,
            'CURRENCYCODE' => 'USD',
            'COUNTRYCODE' => 'US',
            'MAXFAILEDPAYMENTS' => 1,
            'ACCT'=>$request->cc_number,
            'CVV2'=>$request->cc_cvv2,
            'FIRSTNAME'=>$request->first_name,
            'LASTNAME'=>$request->last_name,
            'STREET'=>$request->address,
            'CITY'=>$request->city,
            'STATE'=>$request->state,
            'ZIP'=>$request->zip,
            'EXPDATE'=>$request->cc_expire_date_month . $request->cc_expire_date_year,

        )));

        $response =    curl_exec($curl);
          
        curl_close($curl);
          
        $profilecheck = array();
          
        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $profilecheck[$name] = urldecode($matches['value'][$offset]);
            }
        }
        if (isset($profilecheck['ACK']) && $profilecheck['ACK'] == 'Success') {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, env('PAYPAL_URL'));
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
                'USER'      => env('PAYPAL_USERNAME'),
                'PWD'       => env('PAYPAL_PASSWORD'),
                'SIGNATURE' => env('PAYPAL_SIGNATURE'),
              
              
                'METHOD' => 'ManageRecurringPaymentsProfileStatus',
                'VERSION' => '108',
                
              
                
                'PROFILEID' => Auth::user()->company->profileid,
              
                'ACTION' => 'Cancel'
            )));
              
            $response =    curl_exec($curl);
              
            curl_close($curl);
              
            $cancelcheck = array();
              
            if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
                foreach ($matches['name'] as $offset => $name) {
                    $cancelcheck [$name] = urldecode($matches['value'][$offset]);
                }
            }
            */
            
          $request = array(
            "PARTNER" => "PayPal",
            "VENDOR" => env('PAYPAL_PRO_VENDOR'),
            "USER" => env('PAYPAL_PRO_USERNAME'),
            "PWD" => env('PAYPAL_PRO_PASSWORD'), 
            "TENDER" => "C",
            "TRXTYPE" => "S",
            "CURRENCY" => "USD",
            "AMT" => $discountprice,

            "ACCT" => $request->cc_number,
            "EXPDATE" => $request->cc_expire_date_month.$request->cc_expire_date_year,
            "CVV2" => $request->cc_cvv2,
            'RECURRING' =>'Y',
            "BILLTOFIRSTNAME" => $request->first_name,
            "BILLTOLASTNAME" => $request->last_name,
            "BILLTOSTREET" => $request->address,
            "BILLTOCITY" => $request->city,
            "BILLTOSTATE" => $request->state,
            "BILLTOZIP" => $request->zip,
            "BILLTOCOUNTRY" => 'US',

          );  
              

          //Run request and get the response
          $response = Helper::run_payflow_call($request);
          
            $response_info = array();

             if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $response_info[$name] = urldecode($matches['value'][$offset]);
            }
        }
       

            $json = array();

            if (($response_info['RESULT'] == 0)) {
                date_default_timezone_set("UTC");
       
                $exdate=date('Y-m-d H:i:s', strtotime("+1 Year"));
                $company=Company::find(Auth::user()->company->id);
                $company->expirationdate =  $exdate;
                $company->profileid      =  $profilecheck['PROFILEID'];
                $company->lastpayment    =  $discountprice;
                $company->save();
                $emessage=Dj_email_templates::find(2);
          $cmessage=Helper::change_message_variables($emessage->message,'Credit Card');
          $sent = \Mail::send('email.email', ['cmessage'=>$cmessage], function($message) use($emessage) {
                
            $message->to(Auth::user()->email, Auth::user()->name)
               ->subject($emessage->subject)
                ->from('manager@thephotoboothdirectory.com',"Photo Booth Admin"); });
                $json['success'] = url('signupreview');
            } 
            else {
                $json['error'] = $response_info['RESPMSG'];
            }
       /* }
        else{
            $json['error'] = $profilecheck['RESPMSG'];
        }*/
        echo (json_encode($json));

            
           
        die();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
