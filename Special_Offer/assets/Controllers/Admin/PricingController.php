<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Pricing;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pricing=Pricing::find(1);
        return view('admin.pricing',compact('pricing'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pricing=Pricing::find($id);

        $pricing->pr_free           =   $request->pr_free ;
        $pricing->pr_basic           =   $request->pr_basic  ;
        $pricing->pr_bronze         =   $request->pr_bronze ;
        $pricing->pr_silver         =   $request->pr_silver  ;
        $pricing->pr_gold           =   $request->pr_gold  ;
        $pricing->pr_platinum       =   $request->pr_platinum;

        $pricing->up_free           =   $request->up_free ;
        $pricing->up_basic           =   $request->up_basic ;
        $pricing->up_bronze         =   $request->up_bronze;  
        $pricing->up_silver         =   $request->up_silver;
        $pricing->up_gold           =   $request->up_gold;
        $pricing->up_platinum       =   $request->up_platinum; 

        $pricing->sn_free           =   $request->sn_free;
        $pricing->sn_basic          =   $request->sn_basic; 
        $pricing->sn_bronze         =   $request->sn_bronze;
        $pricing->sn_silver         =   $request->sn_silver;
        $pricing->sn_gold           =   $request->sn_gold  ;
        $pricing->sn_platinum       =   $request->sn_platinum; 

        $pricing->save();
        //Session::flash('alert-success', 'Your Princing Successfully Updated');
        return redirect('admin/pricing');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
