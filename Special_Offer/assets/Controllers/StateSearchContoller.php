<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\Canadazipcodes;
use App\Uszipcodes;
use App\Cms;
use App\States;
//use macro;
class StateSearchContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages=Cms::all();
        $States=States::all();
        return view('statesearch.index',compact('pages','States'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ajax(Request $request)
    {
        //
        $cities=Canadazipcodes::where('State', '=',  $request->state)->groupBy('City')->get();
            
        if($cities->count()==0)

        $cities=Uszipcodes::where('State', '=',  $request->state)->groupBy('City')->get();
        return \Response::json(compact('cities'));
    }
    public function searchcity(Request $request)
    {
        //
         $companies=Company::where('city','=',$request->city)->orderBy('membership', 'DESC')->get();
            
       
        $paginate=0;
        $perpage=0;
        
        return Response::view('ajax.index',compact('companies','paginate','perpage'));
    }
}
