<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Auth;
use App\Company;
use Response;
use URL;
use App\BlogComment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs=Blog::orderBy('id', 'DESC')->get();
        return view('blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->vname=="")
            $name=Auth::user()->first_name.' '.Auth::user()->last_name;
        else
            $name=$request->vname;
        $Blog = new BlogComment;
        $Blog->username       =   $name;
        $Blog->comment        =   $request->comment;
        $Blog->blogid         =   $request->postid;
        
        $Blog->save();
        $blog=Blog::find($request->postid);
        $view=1;
        return Response::view('ajax.blog',compact('blog','view'));
        return redirect('/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(Auth::user('admin')){
        
            $user=Company::find($id);
            $user->user->id;
            
            Auth::loginUsingId("user", $user->user->id);
        
       
            return redirect('/home');
        }
        else{
          return redirect('/');  
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
