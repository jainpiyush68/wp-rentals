<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Company;
use App\States;
use App\Http\Requests;
use App\User;
use App\Metro;
use App\Membership;
use App\Services;
use Session;
use App\Http\Controllers\Controller;
use App\UserServices;
class CompanyEditContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
          $user=Company::find($id);
        
        $memberships=Membership::all();
          $services=Services::where('type','=','service')->orderBy('services', 'Asc')->get();
        $events=Services::where('type','=','event')->orderBy('services', 'Asc')->get();
        
        $States = States::where('country','=',$user->country)->get();
        return view('admin.company.index',compact('user','States','memberships','services','events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $phone=implode('-',$request->phone);
        
        $company = Company::find($id);

        $user= User::find($company->user->id);
        $user->first_name        =   $request->first_name;
        $user->last_name         =   $request->last_name;
        $user->email             =   $request->useremail;
        $user->save();
        $realName=$request->file('logo');

        if(!empty($realName)){

            $rand=rand();

            $imageName = $rand.'.'.$request->file('logo')->getClientOriginalExtension();
           
            $request->file('logo')->move(base_path() . '/public/assets/gallery/'.$company->id.'/', $imageName);

            $company->logo           =   $imageName;

        }
       

        $expiredate='';
        if($request->expirationdate!='')
        $expiredate=$your_date = date("Y-m-d", strtotime($request->expirationdate));

        $company->address        =   $request->address;
        $company->city           =   $request->city;
        $company->state          =   $request->state;
      
        
        $company->zip            =   $request->zip;
        $company->phone          =   $phone;
        $company->name           =   $request->name;
        $company->email          =   $request->email;

        $company->membership     =   $request->membership;
        $company->website_url    =   $request->website_url;
        $company->tagline        =   $request->tagline;
        $company->description    =   $request->description;

        $company->lastpayment    =   $request->lastpayment;
        $company->expirationdate =   $expiredate;
        $company->save();

        $metroid=$request->metroid;
        $Companymembership=$request->membership;
        $state=$request->ctstates;
        $city=$request->city2;

        $services=implode(',', $request->event);
        $UserServices=UserServices::where('companyid','=',$id)->count();
        if($UserServices==0){

            $UserServices= New  UserServices;
            $UserServices->companyid = $id;
            $UserServices->services  = $services;
            $UserServices->save();

        }
        else{

            $UserServices= UserServices::find($company->services->id);
            $UserServices->companyid =$id;
            $UserServices->services =$services;
            $UserServices->save();

        }
       
        //$id=Auth::user()->company->id;
         for($i=0;$i<count($state);$i++) {
 //die();
            if(count($metroid)>$i){

               
                    $metro=Metro::find($metroid[$i]);
                    $metro->companyid       = $id;
                    $metro->state            =$state[$i];
                    $metro->city            =$city[$i];
                    $metro->save();
                
            }
            else{

                $metro=New Metro;
                $metro->companyid       = $id;
                $metro->state            =$state[$i];
                $metro->city            =$city[$i];
                $metro->save();

           }
        }
        //Session::flash('alert-success', 'Thanks for Updating company Infomation');
        if($request->newcompany)
             return redirect('admin/reviewmember');
        else
            return redirect('admin/member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
