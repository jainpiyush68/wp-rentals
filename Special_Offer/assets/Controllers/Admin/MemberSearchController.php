<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\SendMailQueue;
use App\Company;
use Session;
use App\Membership;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\Notes;
class MemberSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $perpage=20;
    public function index(Request $request)
    {
        //
         $name='';
         $membership='';
         $email='';
         $city='';
         if($request->name!='')
           $name=$request->name;
         if($request->membership!='')
           $membership=$request->membership;
         if($request->email!='')
            $email=$request->email;
         if($request->city!='')
            $city=$request->city;

        if(isset($request->search)){
     

            // $Users=User::where('name', '=', $request->name)
            //                 ->where('email','=',$request->email)
            //                 ->wherehas('company', function($q) use($request){$q->where('membership', '=',$request->membership);})
            //                 ->orWhere(function($query) use($request)
            //                     {
            //                         $query->where('name', '=', $request->name)
            //                               ->where('email','=',$request->email);
            //                     })
            //                  ->orWhere(function($query) use($request)
            //                     {
            //                         $query->where('name', '=', $request->name)
            //                               ->whereHas('company', function($q)use($request){$q->where('membership', '=',$request->membership);});
            //                     })
            //                  ->orWhere(function($query) use($request)
            //                     {
            //                         $query->where('email','=',$request->email)
            //                               ->whereHas('company', function($q) use($request){$q->where('membership', '=',$request->membership);});
            //                     })
            //                   ->orWhere(function($query) use($request)
            //                     {
            //                         $query->where('name', '=', $request->name);
            //                     })
            //                    ->orWhere(function($query) use($request)
            //                     {
            //                         $query->where('email','=',$request->email);
                                          
            //                     })
            //                     ->orWhere(function($query) use($request)
            //                     {
            //                         $query->whereHas('company', function($q)use($request){$q->where('membership', '=',$request->membership);});
            //                     })
            //                 ->get();
             
              $Users=Company::where(function($query) use($request)
                                {          if($request->name!='')
                                           $query->where('name','like', '%'.$request->name.'%');
                                           if($request->membership!='')
                                          $query->where('membership', '=',$request->membership);
                                           if($request->email!='')
                                          $query->where('email','=',$request->email);
                                             if($request->city!='')
                                          $query->where('city','=',$request->city);
                                            
                                             if($request->status!='')
                                          $query->where('status','=',$request->status);
                                            
                                            if($request->first!='')
                                          $query->whereHas('user', function($q)use($request){$q->where('first_name', 'like','%'.$request->first.'%');});

                                            if($request->last!='')
                                          $query->whereHas('user', function($q)use($request){$q->where('last_name', 'like','%'.$request->last.'%');});
                                             if($request->state!=''){
                                          //$query->whereHas('cstate', function($q)use($request){$q->where('state', 'like','%'.$request->state.'%');});}
                                           $query->where('state', 'like','%'.$request->state.'%');}
                                          if($request->csdate!='' &&  $request->cedate!='')
                                          $query->whereBetween('created_at',[date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->csdate))),date('Y-m-d 23:59:59',strtotime(str_replace('-','/',$request->cedate)))]);
       if($request->esdate!='' &&  $request->eedate!='')                                                                                
            $query->whereBetween('expirationdate',[date('Y-m-d',strtotime(str_replace('-','/',$request->esdate))),date('Y-m-d',strtotime(str_replace('-','/',$request->eedate)))]);
                                          
if($request->lsdate!='' &&  $request->ledate!=''){
                                               $query->whereHas('user', function($q)use($request){
                                                
                                                    $q->whereBetween('lastlogin',[date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->lsdate))),date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->ledate)))]);
                                                   
                                           });
                                        }


                                })
                           
                            ->orderBy('id', 'DESC')
                             ->get();
  

                            //if(count($Users)==0)
                                //$Users=Company::orderBy('id', 'DESC')->get();

        }
        else{

        $Users=Company::orderBy('id', 'DESC')->get();        }
        $Users2=Company::orderBy('id', 'DESC')->get();  
        $paginate=1;
        $perpage=$this->perpage;
        $Memberships=Membership::all();
         return view('admin.membersearch',compact('Users','Users2','Memberships','paginate','perpage','name','membership','email','city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $company= Company::find($id);
        $user= User::find($company->user->id);
        $company->delete();
        $user->delete();
        


        return redirect('admin\membersearch');
    }
    public function sendmail(Request $request)
    {
        //

        if($request->send){

            $SendMailQueue=new SendMailQueue;

            $SendMailQueue->city                =       $request->city;
            $SendMailQueue->state               =       $request->state;
            $SendMailQueue->membership_type     =       $request->membership;
            $SendMailQueue->subject             =       $request->subject;
            $SendMailQueue->message             =       str_replace ("\n" , '<br/>',$request->message);
            $SendMailQueue->salutation          =       $request->salutation;
            $SendMailQueue->status              =       'pending';

            $SendMailQueue->save();
            //Session::flash('alert-success', 'Your Mail Successfully Submitted.');

        }
        $Memberships=Membership::all();
        return view('admin.massmail',compact('Memberships'));
    }
    public function sendmailcron(Request $request)
    {
        //
        $SendMailQueues=SendMailQueue::where('status','=',"pending")->get();
       foreach ($SendMailQueues as $SendMailQueue) {
       
            $Users=User::where(function($query) use($SendMailQueue){
                            if($SendMailQueue->city!="" ){
                                $query->where('city', '=', $SendMailQueue->city);
                            }
                            if($SendMailQueue->state!="" ){
                                $query->where('state','=',$SendMailQueue->state);
                            }
                            if($SendMailQueue->membership_type!="" ){

                                if($SendMailQueue->membership_type=="all" )
                                    $query->whereHas('company', function($q)use($SendMailQueue){$q->whereIn('membership',[1,2,3,4,5,6]);});
                                else      
                                    $query->whereHas('company', function($q)use($SendMailQueue){$q->where('membership', '=',$SendMailQueue->membership_type);});
                            }

                            })->get();
                foreach ($Users as $user) {
                    $sent = \Mail::send('email.test', ['salutation' => $SendMailQueue->salutation,'body' =>$SendMailQueue->message], function($message) use($user,$SendMailQueue){
                        $message->to($user->email)
                    ->subject('ThePhotoBoothDirectory administration!')
                    ->from('admin@ThePhotoBoothDirectory.com'); 
                    $message->getHeaders()->addTextHeader('Return-Path', 'do-not-reply@ThePhotoBoothDirectory.com');
                    });
                    if($sent)
                    {
                        echo "send mail sucess";
                        $statuschange=SendMailQueue::find($SendMailQueue->id);

                        $statuschange->count++;
                        $statuschange->status= 'send';
                        $statuschange->save();
                    }
                    else{
                        echo "send mail failed";
                    }
                }
         }
          
// $sent = \Mail::send('email.test', ['salutation' => 'hello','body' =>'test'], function($message){
//                         $message->to('jainpiyush68@gmail.com')
//                         ->subject('DiscJockeys administration!')
//                         ->from('admin@discjockeys.com'); });
    }
public function singleusermail(Request $request){
        $cat_id= $request->cid;
        $companies=Company::find($cat_id);
         $notes              =   new Notes;
        $notes->notes       =   $request->body;
        $notes->user_id     =   $companies->user->id;
        $notes->isemail     =   1;
        $notes->save();
        $body=str_replace ("\n" , '<br/>', $request->body);
        $sent = \Mail::send('email.singlemail',['salutation'=>$request->salutation,'body'=>$body], function($message) use($companies,$request){
            //$companies->email;
            $message->to($companies->email, $companies->name)
                ->subject($request->subject)
                ->from('admin@ThePhotoBoothDirectory.com');
            $message->getHeaders()->addTextHeader('Return-Path', 'do-not-reply@ThePhotoBoothDirectory.com');
        });
        if($sent){
            $response = array(
                'msg' => 'success',
            );
        }
        else{
            $response = array(
                'msg' => 'fail',
            );

        }
        return \Response::json(compact('response'));
    }
  public function readmore(Request $request){
           $NoSearchFound="";
         $name='';
         $membership='';
         $email='';
         $city='';
         if($request->name!='')
           $name=$request->name;
         if($request->membership!='')
           $membership=$request->membership;
         if($request->email!='')
            $email=$request->email;
         if($request->city!='')
            $city=$request->city;
        if(isset($request->search)){
  

          
              $Users=Company::where(function($query) use($request)
                                {          if($request->name!='')
                                           $query->where('name','like', '%'.$request->name.'%');
                                           if($request->membership!='')
                                          $query->where('membership', '=',$request->membership);
                                           if($request->email!='')
                                          $query->where('email','=',$request->email);
                                             if($request->city!='')
                                          $query->where('city','=',$request->city);
                                            
                                             if($request->status!='')
                                          $query->where('status','=',$request->status);
                                            
                                            if($request->first!='')
                                          $query->whereHas('user', function($q)use($request){$q->where('name', 'like','%'.$request->first.'%');});

                                            if($request->last!='')
                                          $query->whereHas('user', function($q)use($request){$q->where('name', 'like','%'.$request->last.'%');});
                                             if($request->state!=''){
                                          //$query->whereHas('cstate', function($q)use($request){$q->where('state', 'like','%'.$request->state.'%');});}
                                           $query->where('state', 'like','%'.$request->state.'%');}
                                          if($request->csdate!='' &&  $request->cedate!='')
                                          $query->whereBetween('created_at',[date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->csdate))),date('Y-m-d 23:59:59',strtotime(str_replace('-','/',$request->cedate)))]);
       if($request->esdate!='' &&  $request->eedate!='')                                                                                
            $query->whereBetween('expirationdate',[date('Y-m-d',strtotime(str_replace('-','/',$request->esdate))),date('Y-m-d',strtotime(str_replace('-','/',$request->eedate)))]);
                                          

if($request->lsdate!='' &&  $request->ledate!=''){
                                               $query->whereHas('user', function($q)use($request){
                                                
                                                    $q->whereBetween('lastlogin',[date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->lsdate))),date('Y-m-d 00:00:00',strtotime(str_replace('-','/',$request->ledate)))]);
                                                   
                                           });
                                        }

                                })
                           
                            ->orderBy('id', 'DESC')
                             ->get();
  

                            if(count($Users)==0){ $NoSearchFound="No Search Found";
                                //$Users=Company::orderBy('id', 'DESC')->get();
                            }

        }
        else{

            $Users=Company::orderBy('id', 'DESC')->get();
        }
$response=array();
  $i=0;
foreach($Users as $company):
if(count($company->user)==0)
continue;
  if($company['user_roles']==""):
      $rdate=date_create($company->created_at);
     $regdate=date_format($rdate,"m-d-Y");
$cregdate=date_format($rdate,"Y-m-d");
     $logindate=$company->user->tracker()->orderBy('id','Desc')->first();
      $logindate2=$company->user->UserAction()->where('actionidentifier','=','9')->orderBy('id','Desc')->first();
   if($company->expirationdate=='0000-00-00 00:00:00' || $company->expirationdate==''):
        $expdate='None';
   else:
     $expdate=date_format(date_create($company->expirationdate),"m-d-Y");
  endif;
   $ldate='';
   if(count($logindate2)>0):
    $ldate=date("m-d-Y", strtotime($logindate2->created_at));
  elseif(count($logindate)>0) :
    $ldate=date("m-d-Y", strtotime($logindate->created_at));
  endif;
$first_name=$company->user->first_name;
     $last_name=$company->user->last_name;
  $membership=$company['membership_type']['name'];
   $response[$i][]="<td data-order='$cregdate'>$cregdate</td>";
  $response[$i][]="<td data-order='$cregdate'>$regdate</td>";
             $response[$i][]="<td>$first_name</td>";
             $response[$i][]="<td>$last_name</td>";
              $response[$i][]='<td class="cname">'.$company->name.'</td>';
             $response[$i][]="<td>$company->email</td>";
             $response[$i][]="<td>$company->phone</td>";
             $response[$i][]="<td>$company->state</td>";
            $response[$i][]= "<td>$company->city</td>";
            $response[$i][]= "<td>$ldate</td>";
             $response[$i][]="<td>$membership</td>";
             $response[$i][]="<td>$expdate</td>";
             $response[$i][]='<td class="action" style="text-align:center;">
                <a id="" href="'.url('admin/company/'.$company->id).'"><i id="pen" style="color: #006eb2;text-align:center;padding:3px;" class="fa fa-pencil" aria-hidden="true"></i></a>
                  <a href="'.url('blog/'.$company->id).'" target="_blank"><i id="key" style="color: #31b404;padding:3px;" class="fa fa-key" aria-hidden="true"></i></a> 
                  <a href="javascript:void(0);" class="sendmail" id="'.$company->id.'"><i id="mail" style="color: gray;padding:3px;" class="fa fa-envelope"></i></a> 
                <a href="'.url('admin/member/delete/'.$company->id).'" onclick="return ConfirmDelete();"><i id="del" style="color:#da1f1f;padding:3px;" class="fa fa-times"></i></a>
            </td>';$i++;
               endif;

endforeach;
      return \Response::json(compact('response'));
        
    }  
       
}
