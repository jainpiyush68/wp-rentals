-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2017 at 06:08 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentals`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_tenant_questions`
--

CREATE TABLE `wp_tenant_questions` (
  `ID` int(11) NOT NULL,
  `questions` varchar(255) NOT NULL,
  `isoptional` tinyint(1) NOT NULL,
  `inputtype` varchar(255) NOT NULL,
  `inputkey` varchar(100) NOT NULL,
  `sort_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_tenant_questions`
--

INSERT INTO `wp_tenant_questions` (`ID`, `questions`, `isoptional`, `inputtype`, `inputkey`, `sort_by`) VALUES
(1, 'Name', 0, 'input', 'firstname', 1),
(2, 'Surname', 0, 'input', 'surname', 2),
(3, 'Gender', 1, 'radio', 'gender', 3),
(4, 'Mobile', 0, 'input', 'mobile', 4),
(5, 'E-mail address', 0, 'input', 'email', 6),
(6, 'Skype Name', 0, 'input', 'skypename', 7),
(7, 'Your current job company ', 0, 'input', 'job', 8),
(8, 'Your current field university', 0, 'input', 'university', 9),
(9, 'What is the easiest way to reach you?', 1, 'radio', 'reachyou', 10),
(10, 'Information about the accommodation', 0, 'input', 'accommodation', 11),
(11, 'What kind of accommodation are you looking for?', 0, 'input', 'lookingfor', 12),
(12, 'City / Country you are looking for', 1, 'radio', 'citylookfor', 13),
(13, 'What is your price range? ', 0, 'input', 'pricerange', 14),
(14, 'Your current university/company', 0, 'input', 'company', 15),
(15, 'What is the name of the university/company in the destination city?', 0, 'input', 'desigantioncity', 16),
(16, 'Is there a financial support for the accommodation? (If so please write from who and how) ', 0, 'input', 'financialsupport', 17),
(17, 'Are you preferring districts? (If so please type the names) ', 0, 'input', 'districts', 18),
(18, 'When would you like to move in?', 0, 'input', 'movein', 19),
(19, 'How long are you going to stay?', 0, 'input', 'goingstay', 20),
(20, 'Any other information you would like to share with us?', 0, 'input', 'otherinfo', 21),
(21, 'Would you like to send us your Facebook and LinkedIn name?', 1, 'inputradio', 'faceorlink', 22),
(22, 'Address', 0, 'textarea', 'address', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_tenant_questions`
--
ALTER TABLE `wp_tenant_questions`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_tenant_questions`
--
ALTER TABLE `wp_tenant_questions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
