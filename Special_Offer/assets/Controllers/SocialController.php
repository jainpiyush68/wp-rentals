<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserAction;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('social.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
        $company=Company::find(Auth::user()->company->id);

        $company->facebook    =  $request->fbu;
        $company->youtube     =  $request->ytu;
        $company->twitter     =  $request->twu;
        $company->google      =  $request->gpu;
        $company->myspace     =  $request->msu;
        $company->pinterest   =  $request->pnu;
        $company->instagram   =  $request->igu;
       
        $company->save();
        $normaluser=Auth::user();
        $adminuser=Auth::user('admin');

        if(count($normaluser)>0 && count($adminuser)==0){
            $UserAction                     =       new UserAction;
            $UserAction->user_id            =       Auth::user()->id;
            $UserAction->actionidentifier   =       3;
            $UserAction->save();
        }
        Session::flash('alert-success', 'Your social media Links were saved successfully');
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
